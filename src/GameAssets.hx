package;

import openfl.display.BitmapData;
import openfl.errors.Error;
import openfl.text.Font;
import openfl.Assets;

/**
 * ...
 * @author Igor Skotnikov
 */
class GameAssets
{
	private static var _instance:GameAssets = new GameAssets();
	public var font(default, null):Font;
	public var fontRobotoRegular(default, null):Font;
	
	// img
	public var NO_AVATAR:BitmapData;
	public var NO_AVATAR_BG:BitmapData;
	public var TOP_BAR_BG:BitmapData;
	public var DECK:BitmapData;
	public var CHECKER_BLACK:BitmapData;
	public var CHECKER_WHITE:BitmapData;
	public var CHECKER_FAKE:BitmapData;
	//popUp
	public var TITLE_POPUP:BitmapData;
	public var CONTENT_POPUP:BitmapData;
	public var CONTENT_TOP_POPUP:BitmapData;
	public var CONTENT_BOTTOM_POPUP:BitmapData;
	
	public var dices:Array<BitmapData>;
	
	
	public function new() 
	{
		if (_instance!=null) throw new Error("Singleton and can only be accessed through Singleton.getInstance()");
			//init();
	}
	
	public function init():Void {
		font = Assets.getFont("font/Roboto-Medium.ttf");
		fontRobotoRegular = Assets.getFont("font/Roboto-Regular.ttf");
		NO_AVATAR = Assets.getBitmapData("img/ui/userIcon/UserNoAvatarIcon.png");
		NO_AVATAR_BG = Assets.getBitmapData("img/ui/userIcon/UserIconBG.png");
		DECK = Assets.getBitmapData("img/ui/deck/Deck2.png");
		CHECKER_BLACK = Assets.getBitmapData("img/ui/deck/CheckerBlack.png");
		CHECKER_WHITE = Assets.getBitmapData("img/ui/deck/CheckerWhite.png");
		CHECKER_FAKE = Assets.getBitmapData("img/ui/deck/CheckerFake.png");
		TOP_BAR_BG = Assets.getBitmapData("img/ui/topbarBg/topBarBg.png");
		
		TITLE_POPUP = Assets.getBitmapData("img/ui/popUp/title.png");
		CONTENT_POPUP = Assets.getBitmapData("img/ui/popUp/content.png");
		CONTENT_TOP_POPUP = Assets.getBitmapData("img/ui/popUp/contentTop.png");
		CONTENT_BOTTOM_POPUP = Assets.getBitmapData("img/ui/popUp/contentBottom.png");
		
		dices = [];
		for (i in 1...7) {
			dices.push(Assets.getBitmapData("img/ui/deck/Dice" + i + ".png"));
		}
	}
	
	public static function getInstance():GameAssets {
		return _instance;
	}
}