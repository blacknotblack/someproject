package;

import controller.RESOURCE_LOAD_COMMAND;
import controller.STARTUP_COMPLETE_COMMAND;
import controller.SENDING_PACKET_COMMAND;
import controller.RECEIVED_PACKET_COMMAND;
import controller.viewCommands.SCREEN_BUTTON_TRIGGERED_COMMAND;
import data.manager.popUpManager.PopUpManager;
import govnoknopka.layout.HGroup;
import openfl.display.Sprite;
import openfl.display.StageDisplayState;
import openfl.events.MouseEvent;
import openfl.events.Event;
import openfl.net.Socket;
import openfl.utils.ByteArray;
import service.socket.SocketService;
import view.gamelayer.GameLayer;
import view.popUpLayer.leaveRoomPopUp.LeaveRoomPopUp;
import view.topbar.button.FullScreenButton;
import view.topbar.TopBar;

import service.socket.old.Packet;
import events.GameEvent;
import events.ApplicationEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class Main extends Sprite
{
	
	
	public static var WIDTH:Float = 1920;
	public static var HEIGHT:Float = 1080;
	/* VARS */
	var context:Context;
	var commands:Array<Dynamic>;
	/* CONSTRUCTOR */
	public function new() 
	{
		super();
		
		GameAssets.getInstance().init();
		
		configureContext();
		
		addChild(new TopBar());
		
		addChild(new GameLayer());
		
		PopUpManager.getInstance().init(this);
		
		context.socketService = new SocketService();
		//GlobalEventDispatcher.getInstance().dispatch(new ApplicationEvent(ApplicationEvent.RESOURCE_LOAD));
		//context.socketService.createSocket("localhost", 8081);
		//context.socketService.createSocket("10.44.1.207", 1234);
		//context.socketService.createSocket("46.219.215.111", 1234);
		//soc.send(Packet.createPacketByConstructor(1, 102, { dataforserver:"this is data" } ));
		//var s:Socket = new Socket();
		//var ba:ByteArray = new ByteArray();
		//ba.writeUTFBytes("ffffffffffffffffffffffffffffffff");
		//s.connect("10.44.1.207", 1234);
		//s.writeInt(4545);
		//s.flush();
		//new Test();
	}
	
	private function configureContext():Void {
		context = new Context(this);
		//GlobalEventDispatcher.getInstance().initialize();
		commands = [];
		commands.push(new SENDING_PACKET_COMMAND());
		commands.push(new RECEIVED_PACKET_COMMAND());
		commands.push(new SCREEN_BUTTON_TRIGGERED_COMMAND());
		commands.push(new STARTUP_COMPLETE_COMMAND());
		commands.push(new RESOURCE_LOAD_COMMAND());
	}

}
