package service.socket;

import events.ApplicationEvent;
#if html5
import js.html.ArrayBuffer;
import js.html.FileReader;
import js.html.Uint8Array;
import js.html.WebSocket;
#end

/**
 * ...
 * @author Igor Skotnikov
 */
class SocketService{
	/* VARS */
	private var _host	:String;
	private var _port	:Int;
	#if html5
	private var _socket	:WebSocket;
	private var _buffer	:ArrayBuffer;
	#end
	/* CONSTRUCTOR */		
	public function new() {
		#if html5
		_buffer = new ArrayBuffer(0);
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(ApplicationEvent.SETUP_CONNECTION, SETUP_CONNECTION_HANDLER); 
		#end
	}
	/* API */
	public function createSocket(host:String, port:Int):Void {
		#if html5
		_host = host;
		_port = port;
		
		_socket = new WebSocket('ws://' + host + ':' + port);
		_socket.onerror = CLOSE_HANDLER;
		_socket.onclose = CLOSE_HANDLER;
		_socket.onopen = CONNECT_HANDLER;
		_socket.onmessage = MESSAGE_HANDLER;
		#end
		
	}
	#if html5
	public function send(packet:Packet):Void {
		trace(packet);
		var message:Uint8Array = untyped __js__('pako.deflate({0})', packet.getArrayBufferView());
		var messageLenght:Uint8Array = Packet.intToUint8Array(message.byteLength);
		var bufferArray:Uint8Array = new Uint8Array(new ArrayBuffer(4+message.byteLength));
		for (i in 0...messageLenght.byteLength) {
			bufferArray[i] = messageLenght[i];
		}
		for (i in 0...message.byteLength) {
			bufferArray[i+4] = message[i];
		}
		if(_socket != null && _socket.readyState == WebSocket.OPEN)	_socket.send(bufferArray);
	}
	private function removeSocket():Void{
		if(_socket!=null){
			if(_socket.readyState == WebSocket.OPEN) _socket.close();
			_socket = null;
		}
	}
	private function packetsDivision(uint8Array:Uint8Array):Void {
		var _bufferTemp:ArrayBuffer	= new ArrayBuffer(uint8Array.length + _buffer.byteLength);
		var _bufViewTemp:Uint8Array = new Uint8Array(_bufferTemp);
		
		var __bufferView:Uint8Array = new Uint8Array(_buffer);
		for (i in 0..._buffer.byteLength) {
			_bufViewTemp[i] = __bufferView[i];
		}
		for (i in 0...uint8Array.length) {
			_bufViewTemp[_buffer.byteLength + i] = uint8Array[i];
		}
		
		var buffLenght:UInt = 0;
		if(_bufferTemp.byteLength>=4) buffLenght = (_bufViewTemp[0] << 24) + (_bufViewTemp[1] << 16) + (_bufViewTemp[2] << 8) + (_bufViewTemp[3]);
		
		_buffer = _bufferTemp;

		if (_buffer.byteLength - 4 >= buffLenght) { //4 - длинна пакета, первые 8 бит сокета;
			var bufferToPacket:ArrayBuffer = _buffer.slice(4, buffLenght+4);
			
			bufferToPacket = untyped __js__('pako.inflate({0})', bufferToPacket);
			var packet:Packet = Packet.createPacketByArrayBuffer(bufferToPacket);
			
			if(packet.valid)	GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.RECEIVED_PACKET, 0, null, packet));
			else 				trace("packet ne valid");
			
			_bufferTemp = _buffer.slice(buffLenght + 4, _buffer.byteLength);
			_buffer = new ArrayBuffer(0);
			if(_buffer.byteLength > 0) packetsDivision(new Uint8Array(_bufferTemp));
		}
	}
	
	/* HANDLERS */
	private function SETUP_CONNECTION_HANDLER(e:ApplicationEvent):Void {
		trace(AppData.getInstance().host, AppData.getInstance().port);
		createSocket(AppData.getInstance().host, AppData.getInstance().port);
	}
	
	private function CONNECT_HANDLER(e):Void {
		GlobalEventDispatcher.getInstance().dispatch(new events.ApplicationEvent(events.ApplicationEvent.STARTUP_COMPLETE));
	}
	private function MESSAGE_HANDLER(e):Void {
		var uint8ArrayNew:Uint8Array  = null;
		var arrayBufferNew:ArrayBuffer = null;
		var fileReader     = new FileReader();
		fileReader.onload  = function(progressEvent) {
			arrayBufferNew = fileReader.result;
			uint8ArrayNew  = new Uint8Array(arrayBufferNew);
			packetsDivision(uint8ArrayNew);
			//var abLenght:UInt = (uint8ArrayNew[0] << 24) + (uint8ArrayNew[1] << 16) + (uint8ArrayNew[2] << 8) + (uint8ArrayNew[3]);
			//parseAB(uint8ArrayNew, abLenght);
		};
		fileReader.readAsArrayBuffer(e.data);
	}
	
	function parseAB(bufview:Uint8Array):Void {
		var newBuf:ArrayBuffer = new ArrayBuffer(bufview.byteLength);
		var newbufView = new Uint8Array(newBuf);
		for (i in 0...bufview.byteLength) {
			newbufView[i] = bufview[i + 4];
		}
		newBuf =  untyped __js__('pako.inflate({0})', newBuf);
		newbufView = new Uint8Array(newBuf);
		var str:String = "";
		for (charCode in newbufView) {
			str += charCode + ", ";
		}
		//dispatcher.dispatchEvent(new SocketServiceEvent(SocketServiceEvent.RECEVED, str.substring(10, str.length)));
	}
	
	private function CLOSE_HANDLER(e):Void{
		trace("ERROR : " + e.toString());
		GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.FATAL_ERROR));
	}
	#end
}