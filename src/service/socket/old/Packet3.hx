package service.socket.old;

import js.html.ArrayBuffer;
import js.html.Uint8Array;
import openfl.utils.ByteArray;
import haxe.crypto.Md5;
import haxe.Json;

/**
 * ...
 * @author Igor Skotnikov
 */
class Packet3{
	/* VARS */
	public var userID	:UInt;
	public var command	:UInt;
	public var data		:Dynamic;
	public var valid	:Bool;

	/* CONSTRUCTOR */
	public function new(){
	}
	
	/* API */
	public function getByteArray(compressed:Bool = true):ByteArray{
		var json:String = Json.stringify(data);
		var sign:String = Md5.encode(""+userID+command);
			
		var buf = new ArrayBuffer(4+1+4+json.length+4+sign.length); // 2 bytes for each char
		var bufView = new Uint8Array(buf);
		
		var offset:Int = 0;
		bufView.set(intToUint8Array(userID));
		offset += 4;
		bufView.set(byteToUint8Array(command), offset);
		offset += 1;
		bufView.set(intToUint8Array(json.length),offset);
		offset += 4;
		for (i in 0...json.length) {
			bufView[i+offset] = json.charCodeAt(i);
		}
		offset += json.length;
		bufView.set(intToUint8Array(sign.length), offset);
		offset += 4;
		for (i in 0...sign.length) {
			bufView[i+offset] = sign.charCodeAt(i);
		}
		
		if(compressed) bufView = untyped __js__('pako.deflate({0})', bufView);
		
		//var packetByteArray:ByteArray = ByteArray.fromArrayBuffer(bufView2.buffer);
		var packetByteArray:ByteArray = new ByteArray();
		for (i in 0...bufView.byteLength) {
			packetByteArray.writeByte(bufView[i]);
		}
		return packetByteArray;
	}
	
	private function intToUint8Array(x:Int):Uint8Array {
		var buf = new ArrayBuffer(8);
		var bufView = new Uint8Array(buf);
		bufView[0] = (x >> 24) & 0xFF;
		bufView[1] = (x >> 16) & 0xFF;
		bufView[2] = (x >> 8) & 0xFF;
		bufView[3] = x & 0xFF;
		return bufView;
	}
	private function byteToUint8Array(x:Int):Uint8Array {
		var buf = new ArrayBuffer(1);
		var bufView = new Uint8Array(buf);
		
		bufView[0] = x;
		return bufView;
	}
	
	public static function createPacketByByteArray(byteArray:ByteArray):Packet3{
		var packet:Packet3 = new Packet3();
		
		byteArray.position 	= 0;
		packet.userID 		= byteArray.readUnsignedInt();
		packet.command 		= byteArray.readByte();
		
		var jsonLength:UInt = byteArray.readUnsignedInt();
		var json:String = byteArray.readUTFBytes(jsonLength);
		packet.data = Json.parse(json);
		
		var signLength:UInt = byteArray.readUnsignedInt();
		var sign:String	= byteArray.readUTFBytes(signLength);
		packet.checkSign(sign);
		
		return packet;
	}
	public static function createPacketByConstructor(userID:UInt, command:UInt, data:Dynamic):Packet3{
		var packet:Packet3 = new Packet3();
		
		packet.userID = userID;
		packet.command = command;
		packet.data = data;
		packet.checkSign(Md5.encode(""+packet+packet.command));
		
		return packet;
	}
	public function checkSign(sign:String):Void{
		if(sign == Md5.encode(""+userID+command)) valid = true;
	}
	public function toString(full:Bool = true):String{
		return (full ? '( USERID : '+userID+', ' : '(') + 'COMMAND : '+command+', DATA : '+Json.stringify(data)+ (full ? ', SIGN : '+Md5.encode(""+userID+command)+')' : ')');
	}
}