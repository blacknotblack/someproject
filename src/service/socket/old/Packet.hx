package service.socket.old;

import openfl.utils.ByteArray;
import haxe.crypto.Md5;
import haxe.Json;

/**
 * ...
 * @author Igor Skotnikov
 */
class Packet{
	/* VARS */
	public var userID	:UInt;
	public var command	:UInt;
	public var data		:Dynamic;
	public var valid	:Bool;

	/* CONSTRUCTOR */
	public function new(){
	}
	
	/* API */
	public function getByteArray():ByteArray{
		var packetByteArray:ByteArray = new ByteArray();
		var json:String = Json.stringify(data);
		var sign:String = Md5.encode(""+userID+command);
		
		packetByteArray.writeUnsignedInt(userID);
		packetByteArray.writeByte(command);
		packetByteArray.writeUnsignedInt(json.length);
		packetByteArray.writeUTFBytes(json);
		packetByteArray.writeUnsignedInt(sign.length);
		packetByteArray.writeUTFBytes(sign);
		
		return packetByteArray;
	}
	public static function createPacketByByteArray(byteArray:ByteArray):Packet{
		var packet:Packet = new Packet();
		
		byteArray.position 	= 0;
		packet.userID 		= byteArray.readUnsignedInt();
		packet.command 		= byteArray.readByte();
		
		var jsonLength:UInt = byteArray.readUnsignedInt();
		var json:String = byteArray.readUTFBytes(jsonLength);
		packet.data = Json.parse(json);
		
		var signLength:UInt = byteArray.readUnsignedInt();
		var sign:String	= byteArray.readUTFBytes(signLength);
		packet.checkSign(sign);
		
		return packet;
	}
	public static function createPacketByConstructor(userID:UInt, command:UInt, data:Dynamic):Packet{
		var packet:Packet = new Packet();
		
		packet.userID = userID;
		packet.command = command;
		packet.data = data;
		packet.checkSign(Md5.encode(""+packet+packet.command));
		
		return packet;
	}
	public function checkSign(sign:String):Void{
		if(sign == Md5.encode(""+userID+command)) valid = true;
	}
	public function toString(full:Bool = true):String{
		return (full ? '( USERID : '+userID+', ' : '(') + 'COMMAND : '+command+', DATA : '+Json.stringify(data)+ (full ? ', SIGN : '+Md5.encode(""+userID+command)+')' : ')');
	}
}