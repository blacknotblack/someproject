package service.socket.old;
	
import events.ApplicationEvent;
import openfl.errors.Error;
import openfl.events.Event;
import openfl.events.IEventDispatcher;
import openfl.events.IOErrorEvent;
import openfl.events.ProgressEvent;
import openfl.events.SecurityErrorEvent;
import openfl.net.Socket;
import openfl.utils.ByteArray;

import service.socket.old.Packet;
import service.socket.Packet;
import service.socket.old.Packet3;

/**
 * ...
 * @author Igor Skotnikov
 */
class SocketService{
	/* VARS */
	private var _socket	:Socket;
	private var _host	:String;
	private var _port	:Int;
	private var _buffer	:ByteArray;
	/* CONSTRUCTOR */		
	public function new(){
		_buffer = new ByteArray();
	}
	/* API */
	public function createSocket(host:String, port:Int):Void{
		_host = host;
		_port = port;
		
		//removeSocket();
		
		_socket = new Socket();
		_socket.timeout = 6000;
		_socket.addEventListener(Event.CONNECT, 						CONNECT_HANDLER);
		_socket.addEventListener(Event.CLOSE, 							CLOSE_HANDLER);
		_socket.addEventListener(ProgressEvent.SOCKET_DATA, 			PROGRESS_HANDLER);
		_socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, 	CLOSE_HANDLER);
		_socket.addEventListener(IOErrorEvent.IO_ERROR, 				CLOSE_HANDLER);
		_socket.connect(_host, _port);
		
	}
	public function send(packet:Packet3):Void{
		try{			
			var byteArray:ByteArray = packet.getByteArray();
			//byteArray.compress();
			
			//_socket.writeUnsignedInt(byteArray.length);
			_socket.writeBytes(byteArray);
			_socket.flush();
		}catch(e:Error){
			CLOSE_HANDLER(e);
		}
	}
	private function removeSocket():Void{
		if(_socket!=null){
			
			if(_socket.connected)
				_socket.close();
			
			_socket.removeEventListener(Event.CONNECT, 						CONNECT_HANDLER);
			_socket.removeEventListener(Event.CLOSE, 						CLOSE_HANDLER);
			_socket.removeEventListener(ProgressEvent.SOCKET_DATA, 			PROGRESS_HANDLER);
			_socket.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, 	CLOSE_HANDLER);
			_socket.removeEventListener(IOErrorEvent.IO_ERROR, 				CLOSE_HANDLER);
			
			_socket = null;
		}
	}
	private function packetsDivision():Void{
		try{
			_buffer.position = 0;
			var packetLength:UInt = _buffer.readUnsignedInt();
			if(_buffer.bytesAvailable>=packetLength){
				var packetByteArray:ByteArray = new ByteArray(); 
				_buffer.readBytes(packetByteArray, packetByteArray.position, packetLength);
				//packetByteArray.uncompress();
				
				var packet:Packet = Packet.createPacketByByteArray(packetByteArray);
				
				if(packet.valid) 	GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.RECEIVED_PACKET, 0, null, packet));
				else				trace("RECEIVED", "ERROR : PACKET IS NOT VALID : " + packet.toString());//Cc.errorch("RECEIVED", "ERROR : PACKET IS NOT VALID : " + packet.toString());
				
				var tempBuffer:ByteArray = new ByteArray();
				_buffer.readBytes(tempBuffer, tempBuffer.position, _buffer.bytesAvailable);
				_buffer.clear();
				tempBuffer.readBytes(_buffer, _buffer.length, tempBuffer.bytesAvailable);
				
				if(_buffer.bytesAvailable>0) packetsDivision();
			}
		}catch(e:Dynamic){
			CLOSE_HANDLER(e);
		}
	}
	/* HANDLERS */
	private function CONNECT_HANDLER(e:Event):Void {
		GlobalEventDispatcher.getInstance().dispatch(new ApplicationEvent(ApplicationEvent.STARTUP_COMPLETE));
	}
	private function PROGRESS_HANDLER(e:ProgressEvent):Void{
		_socket.readBytes(_buffer, _buffer.length, _socket.bytesAvailable);
		packetsDivision();
	}	
	private function CLOSE_HANDLER(e:Dynamic = null):Void{
		//Cc.fatal("ERROR : " + e.toString());
		trace("ERROR : " + e.toString());
		GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.FATAL_ERROR));
	}
}