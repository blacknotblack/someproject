package data.mutiplayer;

import events.GameEvent;
import openfl.errors.Error;

/**
 * ...
 * @author Igor Skotnikov
 */
class Room
{	
	private static var _instance:Room = new Room();

	public var gameId:UInt;
	public var activeId:Int;
	public var player:Player;
	public var opponent:Player;
	public var winner:Player;
	public var gameStarted(default, set):Bool;
	
	/* GETTERS AND SETTERS */
	
	private function set_gameStarted(value:Bool):Bool {
		GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.GAME_STARTED, { gameStarted:value } ));
		return gameStarted = value;
	}
	
	/* CONSTRUCTOR */
	public function new() 
	{
		if (_instance != null)  throw new Error("Must be called throw getInstance()");
		player = new Player();
		opponent = new Player();
	}
	
	/* API */
	public function getPlayerByID(id:Float):Player{
		var res:Player;
		if(player.id == id) res = player;
		else res = opponent;
		return res;
	}
	
	public function setWinner(id:Int):Void {
		if (AppData.getInstance().watchingMode == 1) {
			if(player.id == id){
				winner = player;
			}
			if (opponent.id == id) {
				winner = opponent;
			}
			//dispatch(new MultiplayerEvent(MultiplayerEvent.SOMEONE_WINS));
			return;
		}
		if(player.id == id){
			winner = player;
			//dispatch(new MultiplayerEvent(MultiplayerEvent.PLAYER_WINS));
		}
		else if(opponent.id == id){
			winner = opponent;
			//dispatch(new MultiplayerEvent(MultiplayerEvent.OPPONENT_WINS));
		}
	}
	
	public function setActive(id:Int):Void {
		if (id == player.id) {
			player.active = true;
			opponent.active = false;
		}
		else if (id == opponent.id) {
			player.active = false;
			opponent.active = true;
		}
	}
	public function getActivePlayer():Player {
		if(player.active) return player;
		else if (opponent.active) return opponent;
		else return null;
	}
	public static function getInstance():Room {
		return _instance;
	}
	
}