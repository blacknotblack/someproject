package data.mutiplayer;

/**
 * ...
 * @author Igor Skotnikov
 */
class Player
{
	public var id:Int;
	public var name:String;
	public var iconURL(default, set):String;
	public var rating:Float;
	public var balance:Float;
	public var score:Int;
	public var checkerType:Int;
	public var active:Bool;
	public var dice1:Int;
	public var dice2:Int;
	public var dices:Array<Int>;
	public var double:Bool;
	
	/* SETTERS AND GETTERS */
	private function set_iconURL(value:String):String {
        var r:EReg = ~/https/i;
        if(r.match("https")) value = r.replace(value, "http");
		if(value == iconURL) return iconURL;
		return iconURL = value;
	}
	
	/* CONSTRUCTOR */
	public function new() {
		clearCurrentGame();
		createPlayer();
	}
	
	/* API */
	public function createPlayer(id:Int = 0, name:String="", iconURL:String="", rating:Float = 0, balance:Float = 0, score:Int = 0, checkerType:Int = -1):Void{
		this.id = id;
		this.name = name;
		this.iconURL = iconURL;
		this.rating = rating;
		this.balance = balance;
		this.score = score;
		this.checkerType = checkerType;
	}
	
	public function createPlayerByData(data:Dynamic) {
		if (Reflect.hasField(data, "id") && 
			Reflect.hasField(data, "name") &&
			Reflect.hasField(data, "icon") &&
			Reflect.hasField(data, "rating") &&
			Reflect.hasField(data, "balance") &&
			Reflect.hasField(data, "score") &&
			Reflect.hasField(data, "checkerType"))
		{
			createPlayer(Std.parseInt(data.id), data.name, data.icon, Std.parseFloat(data.rating), Std.parseFloat(data.balance), Std.parseInt(data.score), Std.parseInt(data.checkerType));
		}
	}
	public function clearCurrentGame():Void{
		active = false;
		checkerType = -1;
		dice1 = 0;
		dice2 = 0;
		dices = [];
	}
}