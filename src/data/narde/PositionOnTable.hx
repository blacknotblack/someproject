package data.narde;

import openfl.geom.Point;

/**
 * ...
 * @author Igor Skotnikov
 */
class PositionOnTable
{

	public static var position(default, null):Array<Array<Point>> = 
	[[new Point(0,0),		new Point(57.5,0),		new Point(113.7,0), 	new Point(172.5,0), 	new Point(227.5,0), 	new Point(282.5,0), 	new Point(437.5,0), 	new Point(495,0), 		new Point(551,0), 		new Point(607.5,0), 	new Point(665,0), 		new Point(720,0)],
	[new Point(-4,22),		new Point(54, 22), 		new Point(111,22),		new Point(170,22), 		new Point(226,22), 		new Point(282,22), 		new Point(438,22), 		new Point(496,22), 		new Point(553,22), 		new Point(610,22), 		new Point(668,22), 		new Point(723,22)],
	[new Point(-8.7,51),	new Point(50.5,51), 	new Point(108,51), 		new Point(167.5,51), 	new Point(224,51), 		new Point(281,51), 		new Point(439,51), 		new Point(498,51), 		new Point(558,51), 		new Point(613.5,51), 	new Point(672.5,51), 	new Point(729,51)],
	[new Point(-14,80),		new Point(46,80),		new Point(104,80), 		new Point(165,80), 		new Point(222,80), 		new Point(280,80), 		new Point(440,80), 		new Point(500,80), 		new Point(558.5,80), 	new Point(617,80), 		new Point(676,80), 		new Point(734,80)],
	[new Point(-19,110), 	new Point(42,110),		new Point(101,110), 	new Point(162,110), 	new Point(220.5,110), 	new Point(279,110), 	new Point(441.5,110), 	new Point(502,110), 	new Point(561,110), 	new Point(620,110), 	new Point(681,110), 	new Point(739.5,110)],
	[new Point(-24,141.7),	new Point(37.5,141.7),	new Point(97,141.7), 	new Point(159,141.7), 	new Point(218.5,141.7), new Point(278,141.7), 	new Point(442.5,141.7), new Point(504,141.7), 	new Point(564,141.7),	new Point(624.5,141.7), new Point(686,141.7),	new Point(745,141.7)],
	[new Point(-29.5,174),	new Point(33,174), 		new Point(93,174), 		new Point(156,174), 	new Point(216,174), 	new Point(277,174), 	new Point(443,174), 	new Point(506,174), 	new Point(567,174), 	new Point(628.5,174), 	new Point(690.5,174), 	new Point(750,174)],
	[new Point(-35.5,207),	new Point(28,207), 		new Point(89.4,207), 	new Point(152.8,207), 	new Point(214,207), 	new Point(275.5,207), 	new Point(445,207), 	new Point(508,207), 	new Point(570,207), 	new Point(632.5,207), 	new Point(695.5,207), 	new Point(757,207)],
	[new Point(-41,241),	new Point(23,241), 		new Point(85,241), 		new Point(149,241), 	new Point(212,241), 	new Point(274.5,241), 	new Point(446.5,241), 	new Point(510.5,241), 	new Point(573,241), 	new Point(637,241), 	new Point(700,241), 	new Point(763,241)],
	[new Point(-47,276.5),	new Point(18,276.5), 	new Point(81,276.5), 	new Point(146,276.5), 	new Point(210,276.5), 	new Point(273,276.5), 	new Point(447.5,276.5), new Point(512.5,276.5), new Point(576,276.5), 	new Point(641,276.5), 	new Point(706,276.5), 	new Point(769,276.5)],
	[new Point(-53.5,313),	new Point(13,313), 		new Point(77,313), 		new Point(143,313), 	new Point(207.5,313), 	new Point(272,313), 	new Point(449,313),	 	new Point(515,313), 	new Point(580,313), 	new Point(645,313), 	new Point(713,313), 	new Point(775,313)],
	[new Point(-60,350),	new Point(7.5,350), 	new Point(72.5,350), 	new Point(139,350), 	new Point(205,350), 	new Point(271,350), 	new Point(451,350), 	new Point(517,350), 	new Point(583,350), 	new Point(650,350),	 	new Point(717,350), 	new Point(782,350)],
	[new Point(-66.5,389),	new Point(2,389), 		new Point(68,389), 		new Point(135.5,389), 	new Point(203,389), 	new Point(270,389), 	new Point(452,389), 	new Point(520,389), 	new Point(586,389), 	new Point(655,389), 	new Point(722,389), 	new Point(788,389)],
	[new Point(-73,429),	new Point(-4,429), 		new Point(63,429), 		new Point(132,429),	 	new Point(200,429), 	new Point(269,429), 	new Point(454,429), 	new Point(552,429), 	new Point(590,429), 	new Point(659,429), 	new Point(728,429), 	new Point(795,429)],
	[new Point(-80,470),	new Point(-9,470), 		new Point(59,470), 		new Point(128,470), 	new Point(198,470), 	new Point(267,470), 	new Point(455,470), 	new Point(525,470), 	new Point(594,470), 	new Point(665,470), 	new Point(734,470), 	new Point(803,470)],
	[new Point(-87,512),	new Point(-15,512), 	new Point(54,512), 		new Point(124,512), 	new Point(195,512),	 	new Point(266,512), 	new Point(456,512), 	new Point(527,512), 	new Point(598,512), 	new Point(670,512), 	new Point(741,512), 	new Point(810,512)],
	[new Point(-95,556),	new Point(-21,556), 	new Point(49,556), 		new Point(120,556), 	new Point(192,556), 	new Point(264,556), 	new Point(458,556), 	new Point(530,556), 	new Point(602,556), 	new Point(675,556), 	new Point(747,556), 	new Point(818,556)],
	[new Point(-102,600),	new Point(-28,600), 	new Point(43,600),		new Point(115,600), 	new Point(189,600), 	new Point(262,600), 	new Point(460,600), 	new Point(533,600),	 	new Point(606,600), 	new Point(680,600), 	new Point(754,600), 	new Point(826,600)]];
	
	public function new() {	}
	
}