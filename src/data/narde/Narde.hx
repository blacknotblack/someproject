package data.narde;

/**
 * @author Igor Skotnikov
 */

typedef Narde = {
	public var id:Int;
	public var pos:Int;
	public var type:Int;
}