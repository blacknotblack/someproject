package data.narde;

import haxe.Json;
import openfl.errors.Error;

/**
 * ...
 * @author Igor Skotnikov
 */
class DeckModel
{
	private static var _instance:DeckModel = new DeckModel(); 
	//public var deck(default, null):Array<Map<Int, Narde>>;
	public var deck(default, null):Array<Array<Narde>>;
	public var availablePosition:Array<Dynamic>;
	public var headLen(default, null):Int;
	
	/* CONSTRUCTOR */
	public function new() 
	{
		if(_instance!=null) throw new Error("Must be called throw getInstance()");
		deck = [];
		clearCurrentGame();
	}
	
	/* API */
	public function clearCurrentGame():Void {
		for (i in 0...24) {
			deck[i] = [];
		}
		for (i in 0...15) {
			deck[0].push({ id:i, pos:0, type:0 });
		}
		for (i in 0...15) {
			deck[12].push({ id:i+15, pos:12, type:1 });
		}
	}
	
	public function changeCheckerPosition(pos:Int):Void {
		
	}
	
	public function update(data:Dynamic):Void {
		for (i in 0...24) {
			deck[i] = [];
		}
		var fields:Array<String> = Reflect.fields(data);
		for (field in fields) {
			var i:Int = Std.parseInt(field);
			//deck[i] = data[i];
			for (k in 0...data[i].length ) {
				deck[i].push( { id:Std.int(data[i][k].id), pos:Std.int(data[i][k].pos), type:Std.int(data[i][k].type) } );
			}
		}
	}
	
	public function removeCheckerFromPos(checker:Narde):Void {
		for (ch in deck[checker.pos]) {
			if (ch.id == checker.id) {
				deck[checker.pos].remove(ch);
			}
		}
		trace("removeCheckerFromPos  ->" +checker.pos);
	}
	public function addCheckerToPos(checker:Narde,	pos:Int):Void {
		trace("addCheckerToPos  ->" + pos);
		deck[pos].push(checker);
	}
	
	public function moveCheckerToPos(checker:Narde, pos:Int):Void {
		removeCheckerFromPos(checker);
		addCheckerToPos(checker, pos);
	}
	
	public function beforeMoveHeadLen(checkerType:Int):Void {
		if (checkerType == 0) headLen = deck[0].length;
		else headLen = deck[12].length;
	}
	
	public function allInDom(checkerType:Int):Bool {
		var doesAll:Bool = true;
		switch(checkerType) {
			case 0:
				for (i in 0...18) {	
					if(deck[i].length > 0) {
						if (deck[i][0].type == checkerType) doesAll = false;
					}
				}
			case 1:
				for (i in 0...6) {	
					if(deck[i].length > 0) {
						if (deck[i][0].type == checkerType) doesAll = false;
					}
				}
				for (i in 12...24) {	
					if(deck[i].length > 0) {
						if (deck[i][0].type == checkerType) doesAll = false;
					}
				}
		}
		return doesAll;
	}
	
	public static function getInstance():DeckModel {
		return _instance;
	}
}