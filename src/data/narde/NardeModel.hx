package data.narde;

/**
 * ...
 * @author Igor Skotnikov
 */
class NardeModel
{

	var narde:Array<Narde>;
	
	/* CONSTRUCTOR */
	public function new() 
	{
		narde = [];
		clearCurrentGame();
	}
	
	/* API */
	public function setData(data:Dynamic) {
		var id:UInt;
		for (key in data) {
			id = Std.parseInt(key);
			narde[id].id = id;
			narde[id].pos = data[id].pos;
			narde[id].type = data[id].type;
		}
	}
	
	public function clearCurrentGame():Void{
		for (i in 0...15) { //white
			narde[id].id = i;
			narde[id].pos = 1;
			narde[id].type = 0;
		}
		for (i in 15...30) { //black
			narde[id].id = i;
			narde[id].pos = 13;
			narde[id].type = 1;
		}
	}
}