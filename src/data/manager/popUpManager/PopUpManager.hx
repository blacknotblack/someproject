package data.manager.popUpManager;

import openfl.display.Sprite;
import openfl.errors.Error;
import view.popUpLayer.basePopUp.BasePopUp;

/**
 * ...
 * @author Igor Skotnikov
 */
class PopUpManager
{
	/* CONSTANTS */
	private static var _instance:PopUpManager = new PopUpManager();
	/* VARS */
	private var _initialized:Bool;
	private var container:Sprite;
	var map:Map<String, Dynamic>;
	/* CONSTRUCTOR */
	public function new() 
	{
		if (_instance != null)  throw new Error("Must be called throw getInstance()");
	}
	
	public function init(container:Sprite):Void {
		if (!_initialized) {
			this.container = container;
			map = new Map<String, Dynamic>();
			_initialized = true;
		}
		
	}
	/**
	 * 
	 * @param	popUp
	 * @return 	id
	 */
	public function addPopUp(popUp:Dynamic):String {
		var id:String = Type.getClassName(Type.getClass (popUp));
		if (map.exists(id)) return "";
		
		map.set(id, popUp);
		cast(popUp, BasePopUp).show();
		container.addChild(popUp);
		
		return id;
	}
	public function removePopUpByClass(popUp:Class<Dynamic>):Bool {
		var id:String = Type.getClassName(popUp);
		return removePopUpById(id);
	}
	public function removePopUpById(id:String):Bool {
		if (map.exists(id)) {
			container.removeChild(map.get(id));
			map.get(id) == null;
			return map.remove(id);
		}
		else return false;
	}
	
	public static function getInstance():PopUpManager {
		return _instance;
	}
	
}