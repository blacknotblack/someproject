package data.manager.timeoutManager;
/**
 * IntervalData
 * @author Mazurchak D.
 * 21.08.2014
 */
class IntervalData{
	/* VARS */
	public var interval						:Int;
	public var lastUpdateTime(default, set)	:Int;
	public var repeats						:Int;
	public var repeatCount					:Int;
	public var updateFunction				:Dynamic;
	public var updateParams					:Array<Dynamic>;
	/* SETTERS AND GETTERS */

	public function set_lastUpdateTime(value:Int):Int{
		if (lastUpdateTime == value) return value;
		lastUpdateTime = value;
		repeatCount++;
		return value;
	}
	/* CONSTRUCTOR */
	public function new (interval:Int, lastUpdateTime:Int, updateFunction:Dynamic, updateParams:Array<Dynamic>=null, repeats:Int=-1){
		repeatCount = -1;
		
		this.interval 		= interval;
		this.lastUpdateTime = lastUpdateTime;
		this.updateFunction = updateFunction;
		this.updateParams 	= updateParams;
		this.repeats		= repeats;
	}
}