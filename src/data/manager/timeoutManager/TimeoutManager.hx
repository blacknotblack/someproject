package data.manager.timeoutManager;

import data.manager.timeoutManager.IntervalData;
import data.manager.timeoutManager.TimeoutData;
import openfl.events.TimerEvent;
import openfl.utils.Timer;
import openfl.Lib.getTimer;
import openfl.errors.Error;

import data.utils.GUID;

/**
 * TimeoutManager
 * @author Mazurchak D.
 * 16.06.2014
 */
class TimeoutManager{
	/* CONSTANTS */
	private static var _instance:TimeoutManager = new TimeoutManager();
	/* VARS */
	private var _timers			:Map<String, Timer>;
	private var _timeoutData	:Map<String, TimeoutData>;
	private var _intervalData	:Map<String, IntervalData>;
	/* CONSTRUCTOR */
	public function new(){
		if (_instance != null)  throw new Error("Must be called throw getInstance()");
		else		 init();
	}
	public static function getInstance():TimeoutManager{
		return _instance;
	}
	/* API */
	public function setTimeout(duration:Int, completeFunction:Dynamic, completeParams:Array<Dynamic>=null):String{
		var startTime	:Int 	= getTimer();	
		var id			:String = GUID.create();
		
		_timeoutData.set(id, createTimeoutData(startTime, duration, completeFunction, completeParams));
		_timers.set(id, createTimer());
		
		_timers.get(id).start();
		
		return id;
	}
	public function setInterval(interval:Int, updateFunction:Dynamic, updateParams:Array<Dynamic>=null, repeats:Int=-1):String{
		var lastUpdateTime	:Int 	= getTimer();	
		var id				:String = GUID.create();
		
		_intervalData.set(id, createIntervalData(interval, lastUpdateTime, updateFunction, updateParams, repeats));
		_timers.set(id, createTimer());
		
		_timers.get(id).start();

		return id;
	}
	public function clearTimeout(id:String):Void{
		if(_timeoutData.get(id) == null) return;
		
		removeTimer(id);
		removeTimeoutData(id);
	}
	public function clearInterval(id:String):Void{
		if(_intervalData.get(id)==null) return;
		
		removeTimer(id);
		removeIntervalData(id);
	}
	public function finishTimeout(id:String):Void{
		if(_timeoutData.get(id)!=null) Reflect.callMethod(null, _timeoutData[id].completeFunction,  _timeoutData[id].completeParams);
		else				 return;
		
		removeTimer(id);
		removeTimeoutData(id);
	}
	public function clearAllTimeouts():Void {
		var fields = _timers.keys();
		for(field in fields) clearTimeout(field);
	}
	public function finishAllTimeouts():Void{
		var fields = _timers.keys();
		for(field in fields) finishTimeout(field);
	}
	public function clearAllIntervals():Void{
		var fields = _timers.keys();
		for(field in fields) clearInterval(field);
	}
	public function clearAll():Void{
		clearAllIntervals();
		clearAllTimeouts();
	}
	/* PRIVATE */
	private function init():Void{
		_timers 		= new Map<String, Timer>();
		_timeoutData	= new Map<String, TimeoutData>();
		_intervalData	= new Map<String, IntervalData>();
		
	}
	private function createTimeoutData(startTime:Int, duration:Int, completeFunction:Dynamic, completeParams:Array<Dynamic>=null):TimeoutData{
		return new TimeoutData(startTime, duration, completeFunction, completeParams);
	}
	private function createIntervalData(interval:Int, lastUpdateTime:Int, updateFunction:Dynamic, updateParams:Array<Dynamic>=null, repeats:Int=-1):IntervalData{
		return new IntervalData(interval, lastUpdateTime, updateFunction, updateParams, repeats);
	}
	private function removeTimeoutData(id:String):Void{
		if(_timeoutData.get(id)!=null){
			_timeoutData[id] = null;
			_timeoutData.remove(id);
		}
	}
	private function updateInterval(id:String):Void {
		var intervalData:IntervalData = _intervalData[id]; 
		if(intervalData!=null){
			if(intervalData.updateFunction != null) Reflect.callMethod(null, intervalData.updateFunction, intervalData.updateParams);
			intervalData.lastUpdateTime = getTimer();
			if(intervalData.repeats != -1 && intervalData.repeatCount >= intervalData.repeats) clearInterval(id);
		}
	}
	private function removeIntervalData(id:String):Void{
		if(_intervalData.get(id)!=null){
			_intervalData[id] = null;
			_intervalData.remove(id);
		}
	}
	private function createTimer():Timer{
		var timer:Timer = new Timer(10);
	
		timer.addEventListener(TimerEvent.TIMER, TIMER_HANDLER);
		
		return timer;
	}
	private function removeTimer(id:String):Void{
		if(_timers.get(id)!=null){
			_timers[id].reset();
			_timers[id].removeEventListener(TimerEvent.TIMER, TIMER_HANDLER);
			_timers[id] = null;
			_timers.remove(id);
		}
	}
	private function TIMER_HANDLER(e:TimerEvent):Void {
		var curTimer	:Int 	= getTimer();
		var timer		:Timer 	= cast(e.target, Timer);
		var id			:String	= "";
		if(timer!=null){
			var fields = _timers.keys();
			for(field in fields){
				id = field;
				if(_timers[id] == timer){
					if(_timeoutData[id]!=null){
						if(curTimer-_timeoutData[id].startTime>=_timeoutData[id].duration) finishTimeout(id);
					}else if(_intervalData[id]!=null){
						if(curTimer-_intervalData[id].lastUpdateTime>=_intervalData[id].interval) updateInterval(id);
					}
				}
			}
		}
	}
}