package data.const;

/**
 * ...
 * @author Igor Skotnikov
 */
class AppCommands
{	
	inline public static var ERROR_ALREADY_PLAYS:UInt 					= 1;	
	inline public static var ERROR_NOT_REGISTERED:UInt 					= 2;
	inline public static var ERROR_BANNED:UInt 							= 3;
	inline public static var ERROR_NO_MONEY:UInt						= 4;
	inline public static var ERROR_ROOM_IS_FULL:UInt					= 6;
	inline public static var ERROR_WRONG_DATA:UInt						= 7;
	inline public static var ERROR_GAME_BLOCKED:UInt					= 8;

	inline public static var MULTIPLAYER_PLAYER_ADD:UInt 				= 100;
	inline public static var MULTIPLAYER_WAITING_OPPONENT:UInt 			= 101;
	inline public static var MULTIPLAYER_WATCHER_ADD:UInt 				= 102;
	inline public static var MULTIPLAYER_PLAYER_LEAVE_ROOM:UInt			= 103;
	inline public static var MULTIPLAYER_ALL_PLAYERS_LEFT:UInt			= 104;

	inline public static var GAME_PLAYER_IS_READY						= 105;
	inline public static var GAME_BOTH_PLAYERS_ARE_READY				= 106;
	inline public static var GAME_SHAKE_DICE:UInt						= 107;
	inline public static var GAME_CONTINUE:UInt							= 108;
	inline public static var GAME_SHAKE_DICES:UInt						= 109;
	inline public static var GAME_MOVE:UInt								= 110;
	inline public static var GAME_WINNER:UInt							= 111;
	inline public static var TURN_MOVE:UInt								= 112;
	
	public function new() {	}
	
}