package events;
import openfl.events.Event;

/**
 * ...
 * @author Igor Skotnikov
 */
class GameModelEvent extends Event
{	
	inline public static var MOVE_CHECKER_TO_POS:String = 'moveCheckerToPos';
	inline public static var ARRANGE_CHECKERS_ORDER:String = 'arrangeCheckersOrder';

	public var data:Dynamic;

	public function new(type:String, data:Dynamic=null, bubbles:Bool=false, cancelable:Bool=false){
		super(type, bubbles, cancelable);
		this.data = data;
	}

	override public function clone():Event{
		return new GameModelEvent(type, data, bubbles, cancelable);
	}
	
}