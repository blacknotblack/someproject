package events;
import openfl.events.Event;

/**
 * ...
 * @author Igor Skotnikov
 */
class GameEvent extends Event
{

	
	inline public static var DRAW_PLAYER:String = 'drawFirst';
	inline public static var DRAW_OPPONENT:String = 'drawOpponent';
	inline public static var ROTATE_DECK:String = 'rotateDeck';
	inline public static var DRAW_CHECKERS:String = 'drawCheckers';
	
	inline public static var SHOW_ROLLDICE_BUTTON:String = 'showRollDiceButton';
	inline public static var HIDE_ROLLDICE_BUTTON:String = 'hideRollDiceButton';
	inline public static var SHOW_PLAYER_DICES:String = 'showPlayerDices';
	inline public static var HIDE_PLAYER_DICES:String = 'hidePlayerDices';
	inline public static var SHOW_OPPONENT_DICES:String = 'showOpponentDices';
	inline public static var HIDE_OPPONENT_DICES:String = 'hideOpponentDices';
	
	inline public static var ENABLE_USER_CHECKERS:String = 'enableUserCheckers';
	inline public static var DISABLE_USER_CHECKERS:String = 'disableUserCheckers';
	
	inline public static var GLOW_TRAY:String = 'glowTray';
	inline public static var NOT_GLOW_TRAY:String = 'notGlowTray';
	
	inline public static var REMOVE_CHECKER_FROM_DECK_ADD_TO_TRAY:String = 'removeCheckerFromDeck';
	inline public static var GAME_STARTED:String = 'gameStarted';

	public var data:Dynamic;

	public function new(type:String, data:Dynamic=null, bubbles:Bool=false, cancelable:Bool=false){
		super(type, bubbles, cancelable);
		this.data = data;
	}

	override public function clone():Event{
		return new GameEvent(type, data, bubbles, cancelable);
	}
	
}