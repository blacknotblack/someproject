package events;
import openfl.events.Event;

/**
 * ...
 * @author Igor Skotnikov
 */
class FakeLayerEvent extends Event
{	
	inline public static var SHOW_FAKE_CHECKER:String = 'showFakeChecker';
	inline public static var HIDE_FAKE_CHECKER:String = 'hideFakeChecker';

	public var data:Dynamic;

	public function new(type:String, data:Dynamic=null, bubbles:Bool=false, cancelable:Bool=false){
		super(type, bubbles, cancelable);
		this.data = data;
	}

	override public function clone():Event{
		return new FakeLayerEvent(type, data, bubbles, cancelable);
	}
	
}