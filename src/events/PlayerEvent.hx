package events;
import openfl.events.Event;

/**
 * ...
 * @author Igor Skotnikov
 */
class PlayerEvent extends Event
{

	inline public static var PLAYER_BECAME_AN_ACTIVE:String = 'playerBecameAnActive';
	inline public static var OPPONENT_BECAME_AN_ACTIVE:String = 'opponentBecameAnActive';


	public function new(type:String, bubbles:Bool=false, cancelable:Bool=false){
		super(type, bubbles, cancelable);
	}

	override public function clone():Event{
		return new PlayerEvent(type, bubbles, cancelable);
	}
	
}