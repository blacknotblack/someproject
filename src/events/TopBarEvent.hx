package events;
import openfl.events.Event;

/**
 * ...
 * @author Igor Skotnikov
 */
class TopBarEvent extends Event
{
	inline public static var SET_BANK:String = 'setBank';
	inline public static var SET_BALANCE:String = 'setBalance';
	inline public static var SET_GAME_ID:String = 'setGameId';

	public var data:Dynamic;

	public function new(type:String, data:Dynamic=null, bubbles:Bool=false, cancelable:Bool=false){
		super(type, bubbles, cancelable);
		this.data = data;
	}

	override public function clone():Event{
		return new TopBarEvent(type, data, bubbles, cancelable);
	}
	
}