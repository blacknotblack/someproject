package view.playerfield;

import openfl.display.Sprite;
import openfl.text.TextField;
import view.playerfield.icon.UserIcon;
import view.userui.DicesUI;

/**
 * ...
 * @author Igor Skotnikov
 */
class PlayerField extends Sprite
{
	public static var WIDTH = 400;
	public static var HEIGHT = 100;
	
	private var _icon:UserIcon;
	private var _score:TextField;
	private var _userName:TextField;
	
	private var _dices:DicesUI;
	
	public function new() 
	{
		super();
	}
	
	public function showDices(dicesArray:Array<Int>):Void {
		_dices.show(dicesArray);
	}
	public function hideDices():Void {
		_dices.hide();
	}
	
	public function drawField(icon:UserIcon, name:String):Void {
		drawIcon(icon);
		drawDices();
	}
	public function removeField():Void {
		removeIcon();
		removeDices();
	}
	
	private function drawIcon(icon:UserIcon):Void{
		removeIcon();
		
		_icon = icon;
		_icon.alpha = 1;
		addChild(_icon);
	}
	private function removeIcon():Void{
		if(_icon!=null){
			if(contains(_icon)) removeChild(_icon);
			_icon = null;
		}
	}
	private function drawDices():Void{
		removeDices();
		
		_dices = new DicesUI();
		_dices.y = 30;
		_dices.x = -120;
		addChild(_dices);
	}
	private function removeDices():Void{
		if(_dices!=null){
			if(contains(_dices)) removeChild(_dices);
			_dices = null;
		}
	}
	public function dispose():Void{
		removeField();
	}
	
}