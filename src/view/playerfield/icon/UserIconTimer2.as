package view.icon{
	import flash.display.Sprite;
	import flash.text.TextFormat;
	
	import myLib.styles.TextStyle;
	
	import utils.abstract.DisposableSprite;
	
	import view.ui.label.Label;
	
	/**
	 * UserIconTimer2
	 * @author Mazurchak D.
	 * 09.04.2015
	 */
	public class UserIconTimer2 extends DisposableSprite{
		/* VARS */
		private var _label	:Label;
		private var _shown	:Boolean;
		/* SETTERS AND GETTERS */
		public function get shown():Boolean{ return _shown; }
		/* CONSTRUCTOR */
		public function UserIconTimer2(){
			mouseEnabled = mouseChildren = false;
			visible = false;
			
			with(graphics){
				beginFill(0x96171E);
				drawCircle(0, 0, 15);
				endFill();
			}
			
			_label = new Label();
			_label.textStyle = new TextStyle(new TextFormat("PlumbBlackC", 15, 0xFFFFFF), false, true);
			_label.autoFit = true;
			_label.text = "";
			_label.move(-15, -10);
			_label.setSize(30, 20);
			addChild(_label);
		}
		/* API */
		public function show():void{
			GlobalTweener.getInstance().tween(this, {alpha:0}, {alpha:1}, null, 0.3, 0, 0, null, GlobalTweener.setVisible, [this]);
			_shown = true;
		}
		public function hide():void{
			GlobalTweener.getInstance().tween(this, {alpha:1}, {alpha:0}, null, 0.3, 0, 0, null, null, null, GlobalTweener.setInvisible, [this]);
			_shown = false;
		}	
		public function set(time:String):void{
			_label.text = time;
		}
		override public function dispose():void{
			removeChild(_label);
			_label = null;
			
			graphics.clear();
		}
	}
}