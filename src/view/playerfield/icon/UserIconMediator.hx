package view.playerfield.icon;

import data.mutiplayer.Player;
import openfl.display.Loader;
import openfl.display.LoaderInfo;
import openfl.errors.Error;
import openfl.events.Event;
import openfl.events.IOErrorEvent;
import openfl.events.SecurityErrorEvent;
import openfl.net.URLRequest;
import openfl.system.LoaderContext;
import openfl.system.SecurityDomain;

import data.mutiplayer.Room;
	
/**
 * ...
 * @author Igor Skotnikov
 */

class UserIconMediator{

	public var userIcon:UserIcon;

	public function new(userIcon:UserIcon) {
		this.userIcon = userIcon;
		loadIcon();
	}
	
	public function loadIcon():Void {
		var player:Player = Room.getInstance().getPlayerByID(userIcon.id);
		if (player == null || player.iconURL == '') return;
		
		//var iconUrl:String = player.iconURL.replace("https", "http");
		var request:URLRequest 			= new URLRequest(player.iconURL);
		var loader:Loader 				= new Loader();
		var loaderContext:LoaderContext = new LoaderContext(); 
		
		loaderContext.securityDomain 	= SecurityDomain.currentDomain;
		loaderContext.checkPolicyFile 	= true;
		
		loader.contentLoaderInfo.addEventListener(Event.COMPLETE, 						COMPLETE_HANDLER);
		loader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, 	ERROR_HANDLER);
		loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, 				ERROR_HANDLER);
		
		try{
			loader.load(request, loaderContext);
		}catch(e:Error){
			ERROR_HANDLER(e);
		}
	}	
	private function COMPLETE_HANDLER(e:Event):Void{
		var loaderInfo:LoaderInfo 	= cast(e.target, LoaderInfo);
		userIcon.setIcon(loaderInfo.content);
		
		loaderInfo.removeEventListener(Event.COMPLETE, COMPLETE_HANDLER);
	}
	private function ERROR_HANDLER(e:Error):Void{
		trace("ICON LOADER SERVICE ERROR : " + e);
	}
}