package view.playerfield.icon;

import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.DisplayObject;
import openfl.display.Shape;
import openfl.display.Sprite;
import openfl.text.AntiAliasType;
import openfl.text.TextField;
import openfl.text.TextFieldAutoSize;
import openfl.text.TextFormat;
import openfl.Assets;

import openfl.display.PixelSnapping;

/**
 * ...
 * @author Igor Skotnikov
 */	
	
class UserIcon extends Sprite{

	//public var timer(default, null)		:UserIconTimer2;
	public var id(default, null)		:Float;
	public var icon(default, null)		:Bitmap;
	
	private var iconMask	:Shape;
	private var bg			:Bitmap;
	private var nameTF		:TextField;
	
	private var mediator	:UserIconMediator;

	public function new(id:Float) {
		super();
		this.id = id;
		
		drawBG();
		drawTimer();
		drawIcon(new Bitmap(GameAssets.getInstance().NO_AVATAR, PixelSnapping.AUTO, true));
		mediator = new UserIconMediator(this);
	}

	public function setIcon(icon:DisplayObject):Void{
		drawIcon(icon);
	}
	public function setTimer(time:String, percent:Float):Void{
		//if(_timer){
			//_timer.set(time);
			//
			//if(!_timer.visible) _timer.show();
			//if(percent == 1) 	_timer.hide();
		//}
	}
	public function dispose():Void{
		wipeIcon();
		wipeTimer();
		wipeBG();
	}

	private function drawBG():Void{
		wipeBG();
		
		bg	= new Bitmap(GameAssets.getInstance().NO_AVATAR_BG, PixelSnapping.AUTO, true);
		
		addChild(bg);
	}
	private function wipeBG():Void{
		if(bg!=null){
			if(contains(bg)) removeChild(bg);
			bg = null;
		}
	}
	private function drawTimer():Void{
		//wipeTimer();
		//
		//_timer = new UserIconTimer2();
		//_timer.x = 0;
		//_timer.y = 100;
		//
		//addChild(_timer);
	}
	private function wipeTimer():Void{
		//if(timer!=null){
			//if(contains(_timer)) removeChild(timer);
			//timer.dispose();
			//timer = null;
		//}
	}
	private function drawIcon(icon:DisplayObject):Void{
		wipeIcon();
		
		icon.width 	= 80;
		icon.height = 100;
		
		iconMask = new Shape();
		iconMask.graphics.beginFill(0x00FF00);
		iconMask.graphics.drawRoundRect(4, 4, 72, 92, 5);
		iconMask.graphics.endFill();
		
		icon.mask = iconMask;
		
		addChildAt(icon, getChildIndex(bg) + 1);
		addChildAt(iconMask, getChildIndex(icon) + 1);
	}
	private function wipeIcon():Void{
		if(icon!=null){
			icon.mask = null;
			if(contains(icon)) removeChild(icon);
			icon = null;
			
		}
		if(iconMask!=null){
			if(contains(iconMask)) removeChild(iconMask);
			iconMask = null;
		}
	}
}