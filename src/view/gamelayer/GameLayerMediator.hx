package view.gamelayer;

import data.mutiplayer.Room;
import data.narde.DeckModel;
import events.GameEvent;
import events.ApplicationEvent;
import flash.display.Sprite;
import openfl.events.EventDispatcher;
import view.playerfield.icon.UserIcon;
import view.playerfield.PlayerField;
import openfl.events.Event;
import view.table.Deck;
import view.userui.RollDiceButton;

/**
 * ...
 * @author Igor Skotnikov
 */
class GameLayerMediator
{
	/* VARS */
	private var gameLayer:GameLayer;
	
	private var playerField:PlayerField;
	private var opponentField:PlayerField;
	private var deck:Deck;
	private var rollDiceButton:RollDiceButton;
	
	//-- inject --//
	private var room:Room;
	private var eventDispatcher:EventDispatcher;
	private var contextview:Sprite;
	
	/* CONSTRUCTOR */
	public function new(gameLayer:GameLayer) 
	{
		this.gameLayer = gameLayer;
		
		room = Room.getInstance();
		contextview = Context.getInstance().contextview;
		
		setupListeners();
		setupDeck();
	}
	
	private function setupListeners():Void {
		eventDispatcher = GlobalEventDispatcher.getInstance().eventDispatcher;
		eventDispatcher.addEventListener(GameEvent.DRAW_PLAYER,		DRAW_PLAYER_HANDLER);
		eventDispatcher.addEventListener(GameEvent.DRAW_OPPONENT,	DRAW_OPPONENT_HANDLER);
		
		eventDispatcher.addEventListener(GameEvent.SHOW_PLAYER_DICES,		SHOW_PLAYER_DICES_HANDLER);
		eventDispatcher.addEventListener(GameEvent.HIDE_PLAYER_DICES,		HIDE_PLAYER_DICES_HANDLER);
		eventDispatcher.addEventListener(GameEvent.SHOW_OPPONENT_DICES,		SHOW_OPPONENT_DICES_HANDLER);
		eventDispatcher.addEventListener(GameEvent.HIDE_OPPONENT_DICES,		HIDE_OPPONENT_DICES_HANDLER);
		
		eventDispatcher.addEventListener(GameEvent.SHOW_ROLLDICE_BUTTON,	SHOW_ROLLDICE_BUTTON_HANDLER);
		eventDispatcher.addEventListener(GameEvent.HIDE_ROLLDICE_BUTTON,	HIDE_ROLLDICE_BUTTON_HANDLER);
		
		contextview.stage.addEventListener(Event.RESIZE, RESIZE_HANDLER);
	}
	
	/* HANDLERS */
	private function DRAW_PLAYER_HANDLER(e:GameEvent):Void {
		trace("DRAW_PLAYER_HANDLER");
		showDeck();
		showPlayerField();
		setupRollDiceButton();
	}
	private function DRAW_OPPONENT_HANDLER(e:GameEvent):Void {
		trace("DRAW_OPPONENT_HANDLER");
		if(room.opponent.id!=0) showOpponentField();
	}
	private function SHOW_ROLLDICE_BUTTON_HANDLER(e:GameEvent):Void {
		rollDiceButton.show();
	}
	private function HIDE_ROLLDICE_BUTTON_HANDLER(e:GameEvent):Void {
		rollDiceButton.hide();
	}
	
	private function RESIZE_HANDLER(e:Event):Void {
		var factorX:Float = contextview.stage.stageWidth / Main.WIDTH;
		var factorY:Float = (contextview.stage.stageHeight - 2*PlayerField.HEIGHT) / Main.HEIGHT;
		var factor:Float = (factorX < factorY)?factorX:factorY;
		if(playerField!=null){	
			playerField.x = (contextview.stage.stageWidth - PlayerField.WIDTH) / 2;
			playerField.y = contextview.stage.stageHeight - PlayerField.HEIGHT;
		}
		if(opponentField!=null){	
			opponentField.x = (contextview.stage.stageWidth - PlayerField.WIDTH) / 2;
			opponentField.y = 0;
		}
		if(deck!=null){
			deck.scaleX = deck.scaleY = factor;
			deck.x = (contextview.stage.stageWidth - deck.width) / 2 + 220 * factor;
			deck.y = (contextview.stage.stageHeight - deck.height) / 2 + 63 * factor;
		}
		if (rollDiceButton != null) {
			rollDiceButton.x = playerField.x + PlayerField.WIDTH + 100 * factor;
			rollDiceButton.y = contextview.stage.stageHeight - PlayerField.HEIGHT;
		}
	}
	private function SHOW_PLAYER_DICES_HANDLER(e:GameEvent):Void {
		playerField.showDices(room.player.dices);
	}
	private function HIDE_PLAYER_DICES_HANDLER(e:GameEvent):Void {
		playerField.hideDices();
	}
	private function SHOW_OPPONENT_DICES_HANDLER(e:GameEvent):Void {
		opponentField.showDices(room.opponent.dices);
	}
	private function HIDE_OPPONENT_DICES_HANDLER(e:GameEvent):Void {
		opponentField.hideDices();
	}
	
	/* API */
	/*   player   */
	private function showPlayerField():Void{
		if(playerField==null) setupPlayerField();
		
		playerField.drawField(new UserIcon(room.player.id), room.player.name);
	}
	private function hidePlayerField():Void{
	}
	private function setupPlayerField():Void {
		removePlayerField();
		playerField = new PlayerField();
		playerField.x = (contextview.stage.stageWidth - PlayerField.WIDTH) / 2;
		playerField.y = contextview.stage.stageHeight - PlayerField.HEIGHT;
		gameLayer.addChild(playerField);
	}
	private function removePlayerField():Void{
		if(playerField!=null){
			if(gameLayer.contains(playerField)) gameLayer.removeChild(playerField);
			playerField.dispose();
			playerField = null;
		}
	}
	
	private function showRollDice():Void{
		if(rollDiceButton==null) setupRollDiceButton();
		
		rollDiceButton.show();
	}
	private function hideRollDiceButton():Void {
		rollDiceButton.hide();
	}
	private function setupRollDiceButton():Void {
		removeRollDiceButton();
		rollDiceButton = new RollDiceButton();
		rollDiceButton.x = playerField.x+PlayerField.WIDTH+100;
		rollDiceButton.y = contextview.stage.stageHeight - PlayerField.HEIGHT;
		gameLayer.addChild(rollDiceButton);
	}
	private function removeRollDiceButton():Void{
		if(rollDiceButton!=null){
			if(gameLayer.contains(rollDiceButton)) gameLayer.removeChild(rollDiceButton);
			rollDiceButton.dispose();
			rollDiceButton = null;
		}
	}
	/*   opponent   */
	private function showOpponentField():Void{
		if(opponentField==null) setupOpponentField();
		opponentField.drawField(new UserIcon(room.opponent.id), room.opponent.name);
	}
	private function hideOpponentField():Void{
	}
	private function setupOpponentField():Void {
		removeOpponentField();
		opponentField = new PlayerField();
		opponentField.x = (contextview.stage.stageWidth - PlayerField.WIDTH) / 2;
		opponentField.y = 0;
		gameLayer.addChild(opponentField);
	}
	private function removeOpponentField():Void{
		if(opponentField!=null){
			if(gameLayer.contains(opponentField)) gameLayer.removeChild(opponentField);
			opponentField.dispose();
			opponentField = null;
		}
	}
	/*  table  */
	private function setupDeck():Void{
		removeDeck();
			
		deck = new Deck();
		var factorX:Float = contextview.stage.stageWidth / Main.WIDTH;
		var factorY:Float = (contextview.stage.stageHeight) / Main.HEIGHT;
		var factor:Float = (factorX < factorY)?factorX:factorY;
		deck.scaleX = deck.scaleY = factor;
		deck.x = (contextview.stage.stageWidth - deck.width) / 2 + 220 * factor;
		deck.y = (contextview.stage.stageHeight - deck.height) / 2 + 63 * factor;
		gameLayer.addChild(deck);
	}
	private function removeDeck():Void{
		if(deck!=null){
			if(gameLayer.contains(deck)) gameLayer.removeChild(deck);
			deck.dispose();
			deck = null;
		}
	}
	private function showDeck():Void {
		if(deck==null) setupDeck();
		deck.show();	
	}
	
}