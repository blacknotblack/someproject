package view.gamelayer;

import openfl.display.Sprite;

/**
 * ...
 * @author Igor Skotnikov
 */
class GameLayer extends Sprite
{
	private var gameLayerMediator:GameLayerMediator;
	public function new() 
	{
		super();
		gameLayerMediator = new GameLayerMediator(this);
	}
	
}