package view.popUpLayer.popUpButton;

import govnoknopka.button.Button;
import openfl.geom.Rectangle;
import openfl.text.TextFormat;
import view.popUpLayer.popUpButton.skin.RedSkinPopUpButton;

/**
 * ...
 * @author Igor Skotnikov
 */
class RedPopUpButton extends Button 
{
	public function new(text:String) 
	{
		super(new RedSkinPopUpButton(), new Rectangle(0,0,800,480), new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, 12, 0xFFFFFF), text);
		buttonMode = true;
		setSize(55, 20);
	}

}
