package view.popUpLayer.popUpButton.skin;

import govnoknopka.skin.SkinButton;
import openfl.Assets;
import openfl.display.Bitmap;

/**
 * ...
 * @author Igor Skotnikov
 */
class GreenSkinPopUpButton extends SkinButton
{
	public function new() 
	{
		super();
		skinOver = new Bitmap(Assets.getBitmapData("img/ui/button/GreenButtonOverAsset.png"));
		skinUp = new Bitmap(Assets.getBitmapData("img/ui/button/GreenButtonUpAsset.png"));
		skinDown = new Bitmap(Assets.getBitmapData("img/ui/button/GreenButtonDownAsset.png"));
	}
	
}