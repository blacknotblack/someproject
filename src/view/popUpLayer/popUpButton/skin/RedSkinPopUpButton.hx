package view.popUpLayer.popUpButton.skin;

import govnoknopka.skin.SkinButton;
import openfl.Assets;
import openfl.display.Bitmap;

/**
 * ...
 * @author Igor Skotnikov
 */
class RedSkinPopUpButton extends SkinButton
{
	public function new() 
	{
		super();
		skinOver = new Bitmap(Assets.getBitmapData("img/ui/button/RedButtonOverAsset.png"));
		skinUp = new Bitmap(Assets.getBitmapData("img/ui/button/RedButtonUpAsset.png"));
		skinDown = new Bitmap(Assets.getBitmapData("img/ui/button/RedButtonDownAsset.png"));
	}
	
}