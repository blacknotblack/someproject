package view.popUpLayer.popUpButton;

import govnoknopka.button.Button;
import openfl.geom.Rectangle;
import openfl.text.AntiAliasType;
import openfl.text.TextFormat;
import view.popUpLayer.popUpButton.skin.GreenSkinPopUpButton;

/**
 * ...
 * @author Igor Skotnikov
 */
class GreenPopUpButton extends Button 
{
	public function new(text:String) 
	{
		super(new GreenSkinPopUpButton(), new Rectangle(0,0,800,480), new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, 12, 0xFFFFFF), text);
		buttonMode = true;
		textfield.antiAliasType = AntiAliasType.ADVANCED;
		textfield.sharpness = 200;
		setSize(55, 20);
	}

}
