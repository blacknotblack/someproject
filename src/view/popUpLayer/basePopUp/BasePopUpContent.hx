package view.popUpLayer.basePopUp;
import openfl.display.Bitmap;
import openfl.display.PixelSnapping;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFieldAutoSize;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;
import view.popUpLayer.popUpButton.GreenPopUpButton;
import view.popUpLayer.popUpButton.RedPopUpButton;

/**
 * ...
 * @author Igor Skotnikov
 */
class BasePopUpContent extends Sprite
{
	private var contentTopBitmap:Bitmap;
	private var contentBottomBitmap:Bitmap;
	private var contentTF:TextField;
	
	private var okButton:GreenPopUpButton;
	private var cancelButton:RedPopUpButton;
	
	public function new(text:String)
	{
		super();
		contentTopBitmap = new Bitmap(GameAssets.getInstance().CONTENT_TOP_POPUP, PixelSnapping.ALWAYS, true);
		addChild(contentTopBitmap);
		contentBottomBitmap = new Bitmap(GameAssets.getInstance().CONTENT_BOTTOM_POPUP, PixelSnapping.ALWAYS, true);
		addChild(contentBottomBitmap);
		
		contentTF = new TextField();
		contentTF.defaultTextFormat = new TextFormat(GameAssets.getInstance().font.fontName, 14, 0x4C4C4C, true, null, null, null, null, TextFormatAlign.JUSTIFY);
		contentTF.embedFonts = true;
		contentTF.selectable = true;
		contentTF.wordWrap = true;
		contentTF.multiline = true;
		contentTF.text = text;
		//contentTF.border = true;
		contentTF.autoSize = TextFieldAutoSize.LEFT;
		contentTF.width = contentTopBitmap.width - 20;
		contentTF.x = (contentTopBitmap.width - contentTF.width) / 2;
		contentTF.y = 10;
		addChild(contentTF);
		
		contentTopBitmap.height = contentTF.height + 20;
		if (contentTopBitmap.height < 70) contentTopBitmap.height = 70;
		contentBottomBitmap.y = contentTopBitmap.height;
	}
	
}