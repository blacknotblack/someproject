package view.popUpLayer.basePopUp;

import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFieldAutoSize;
import view.popUpLayer.basePopUp.BasePopUpContent;
import data.manager.popUpManager.PopUpManager;

import openfl.display.PixelSnapping;
import motion.Actuate;
import motion.easing.Quad;

/**
 * ...
 * @author Igor Skotnikov
 */
class BasePopUp extends Sprite
{
	private var titleTF:TextField;
	private var content:BasePopUpContent;
	
	private var titleBitmap:Bitmap;
	//private var contentBitmap:Bitmap;
	private var container:Sprite;
	public function new(title:String, content:BasePopUpContent) 
	{
		super();
		addChild(container);

		titleBitmap = new Bitmap(GameAssets.getInstance().TITLE_POPUP, PixelSnapping.ALWAYS, true);
		addChild(titleBitmap);
		//contentBitmap = new Bitmap(GameAssets.getInstance().CONTENT_POPUP, PixelSnapping.ALWAYS, true);
		//contentBitmap.y = titleBitmap.height;
		//addChild(contentBitmap);
		
		titleTF = new TextField();
		titleTF.defaultTextFormat = new TextFormat(GameAssets.getInstance().font.fontName, 14, 0x4C4C4C, true);
		titleTF.embedFonts = true;
		titleTF.selectable = false;
		titleTF.autoSize = TextFieldAutoSize.CENTER;
		titleTF.text = title;
		titleTF.x = (titleBitmap.width - titleTF.width) / 2;
		titleTF.y = (titleBitmap.height - titleTF.height) / 2;
		addChild(titleTF);
		
		content.y = titleBitmap.height;
		addChild(content);
		
	}
	
	public function show():Void {
		scaleX = scaleY = 0.3;
		x = (Context.getInstance().contextview.stage.stageWidth - this.width) / 2;
		y = (Context.getInstance().contextview.stage.stageHeight - this.height) / 2;	
		Actuate.tween(this, 0.1, { scaleX : 1, scaleY:1 } ).onUpdate(function () {
			x = (Context.getInstance().contextview.stage.stageWidth - this.width) / 2;
			y = (Context.getInstance().contextview.stage.stageHeight - this.height) / 2;		
		}).ease(Quad.easeIn);
	}
	public function close():Void {
		Actuate.tween(this, 0.1, { scaleX : 0.3, scaleY:0.3 } ).onUpdate(function() {
			x = (Context.getInstance().contextview.stage.stageWidth - this.width) / 2;
			y = (Context.getInstance().contextview.stage.stageHeight - this.height) / 2;		
		}).onComplete(function(){
			PopUpManager.getInstance().removePopUpByClass(Type.getClass (this));
		}).ease(Quad.easeIn);
	}
	
}