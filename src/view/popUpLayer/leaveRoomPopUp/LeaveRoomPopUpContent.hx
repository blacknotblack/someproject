package view.popUpLayer.leaveRoomPopUp;

import js.html.Event;
import service.socket.SocketServiceEvent;
import view.popUpLayer.basePopUp.BasePopUpContent;
import view.popUpLayer.popUpButton.GreenPopUpButton;
import view.popUpLayer.popUpButton.RedPopUpButton;
import openfl.events.MouseEvent;
import data.mutiplayer.Room;
import data.const.AppCommands;

/**
 * ...
 * @author Igor Skotnikov
 */
class LeaveRoomPopUpContent extends BasePopUpContent
{
	public function new(text:String)
	{
		super(text);
		okButton = new GreenPopUpButton(Localization.getInstance().data.leaveRoomPopUpButton1);
		addChild(okButton);
		cancelButton = new RedPopUpButton(Localization.getInstance().data.leaveRoomPopUpButton2);
		addChild(cancelButton);
		
		okButton.x = contentBottomBitmap.width / 2 -okButton.width -30;
		okButton.y = contentBottomBitmap.y;
		cancelButton.x = contentBottomBitmap.width / 2 +30;
		cancelButton.y = contentBottomBitmap.y;
		
		okButton.addEventListener(MouseEvent.MOUSE_UP, MOUSE_UP_HANDLER);
		cancelButton.addEventListener(MouseEvent.MOUSE_UP, MOUSE_UP_HANDLER);
	}
	
	private function MOUSE_UP_HANDLER(e:MouseEvent):Void {
		if(e.target == okButton){
			if(Room.getInstance().gameStarted){
				GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.SENDING_PACKET, AppCommands.MULTIPLAYER_PLAYER_LEAVE_ROOM));
			}
			//else	jsManager.callJS(JSManager.BACK_TO_MENU);
		}
		cast(parent, LeaveRoomPopUp).close();
	}
}