package view.popUpLayer.leaveRoomPopUp;
import view.popUpLayer.basePopUp.BasePopUp;

/**
 * ...
 * @author Igor Skotnikov
 */
class LeaveRoomPopUp extends BasePopUp
{

	public function new() 
	{
		super(Localization.getInstance().data.leaveRoomPopUpTitle, new LeaveRoomPopUpContent(Localization.getInstance().data.leaveRoomPopUpContentText2));
	}
	
}