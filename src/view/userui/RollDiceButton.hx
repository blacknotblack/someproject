package view.userui;

import govnoknopka.button.Button;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.PixelSnapping;
import openfl.geom.Rectangle;
import openfl.text.TextFormat;
import view.topbar.skin.BlackSkinButton;

/**
 * ...
 * @author Igor Skotnikov
 */
class RollDiceButton extends Button 
{

	var mediator:RollDiceButtonMediator;
	public function new() 
	{
		super(new BlackSkinButton(), new Rectangle(0,0,800,480), new TextFormat(GameAssets.getInstance().font.fontName, 12, 0xFFFF00));
		this.setSize(60, 30);
		this.text = Localization.getInstance().data.rollDiceButtonLabel1;
		toolTip = createToolTip(new TextFormat(GameAssets.getInstance().font.fontName, 12, 0x222222), Localization.getInstance().data.rollDiceButtonToolTip1);
		
		mediator = new RollDiceButtonMediator(this);
		
		//alpha = 0;
		visible = false;
	}
	
	public function show():Void {
		//alpha = 1;
		visible = true;
	}
	public function hide():Void {
		//alpha = 0;
		visible = false;
	}

	public function changeText(started:Bool):Void {
		if (started) {
			this.text = Localization.getInstance().data.rollDiceButtonLabel2;
			toolTip.text = Localization.getInstance().data.rollDiceButtonToolTip2;
		}
		else {
			this.text = Localization.getInstance().data.rollDiceButtonLabel1;
			toolTip.text = Localization.getInstance().data.rollDiceButtonToolTip1;
		}
	}
	
	public function dispose():Void {
		mediator.dispose();
		mediator = null;
	}	
}