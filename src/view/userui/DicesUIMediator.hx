package view.userui;

import openfl.events.EventDispatcher;
import events.GameEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class DicesUIMediator
{

	/* VARS */
	private var dicesUI:DicesUI;
	private var eventDispather:EventDispatcher;
	
	/* CONSTRUCTOR */
	public function new(dicesUI:DicesUI) 
	{
		this.dicesUI = dicesUI;
		setupListeners();
	}
	
	/* API */
	private function setupListeners():Void {
		eventDispatcher = GlobalEventDispatcher.getInstance().eventDispatcher;
		//eventDispatcher.addEventListener(GameEvent.SHOW_DICES,	SHOW_DICES_HANDLER);
		//eventDispatcher.addEventListener(GameEvent.HIDE_DICES,	HIDE_DICES_HANDLER);
	}
	
	/* HANDLERS */
	private function SHOW_DICES_HANDLER(e:GameEvent):Void {
		dicesUI.show()
	}
	private function SHOW_DICES_HANDLER(e:GameEvent):Void {
		dicesUI.hide();
	}
	public function dispose():Void {
		eventDispatcher.removeEventListener(GameEvent.SHOW_DICES,	SHOW_DICES_HANDLER);
		eventDispatcher.removeEventListener(GameEvent.HIDE_DICES,	HIDE_DICES_HANDLER);
		dicesUI = null;
		eventDispather = null;
	}
	
}