package view.userui;
import govnoknopka.button.Button;
import openfl.events.MouseEvent;
import service.socket.SocketServiceEvent;
import data.const.AppCommands;
import data.mutiplayer.Room;
import view.userui.RollDiceButton;
import events.GameEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class RollDiceButtonMediator
{

	var view:RollDiceButton;
	public function new(view:RollDiceButton) 
	{
		this.view = view;
		init();
	}
	
	inline private function init() {
		view.addEventListener(MouseEvent.MOUSE_UP, MOUSE_HANDLER);
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(GameEvent.GAME_STARTED, GAME_STARTED_HANDLER);
	}
	
	private function MOUSE_HANDLER(e:MouseEvent):Void {
		if (Room.getInstance().gameStarted) {
			GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.SENDING_PACKET, AppCommands.GAME_SHAKE_DICES));
		}
		else {	
			GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.SENDING_PACKET, AppCommands.GAME_SHAKE_DICE));
		}
		view.hide();
	}
	private function GAME_STARTED_HANDLER(e:GameEvent):Void {
		view.changeText(e.data.gameStarted);
	}
	
	public function dispose():Void {
		view.removeEventListener(MouseEvent.MOUSE_UP, MOUSE_HANDLER);
		GlobalEventDispatcher.getInstance().eventDispatcher.removeEventListener(GameEvent.GAME_STARTED, GAME_STARTED_HANDLER);
	}
}