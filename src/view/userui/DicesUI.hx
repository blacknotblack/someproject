package view.userui;

import openfl.display.Sprite;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.PixelSnapping;
import openfl.text.TextFormat;

/**
 * ...
 * @author Igor Skotnikov
 */
class DicesUI extends Sprite
{

	//private var mediator:DicesUIMediator;
	private var dice1:Bitmap;
	private var dice2:Bitmap;
	public function new() 
	{
		super();
		//mediator = new DicesUIMediator(this);
		alpha = 0;
	}
	
	public function show(dicesArray:Array<Int>):Void {
		removeDices();
		
		dice1 = new Bitmap(GameAssets.getInstance().dices[dicesArray[0] - 1], PixelSnapping.ALWAYS, true);
		dice1.width = dice1.height = 50;
		addChild(dice1);
		if (dicesArray.length > 1) {
			dice2 = new Bitmap(GameAssets.getInstance().dices[dicesArray[1] - 1], PixelSnapping.ALWAYS, true);
			dice2.width = dice2.height = 50;
			dice2.x = 55;
			addChild(dice2);
		}	
		alpha = 1;
	}
	public function hide():Void {
		alpha = 0.7;
	}
	public function cleareCurrentGame():Void {
		removeDices();
		alpha = 0;
	}

	public function dispose():Void {
		removeDices();
		//mediator.dispose();
		//mediator = null;
	}
	private function removeDices():Void {
		if (dice1 != null) {
			if (contains(dice1)) removeChild(dice1);
			dice1 = null;
		}
		if (dice2 != null) {
			if (contains(dice2)) removeChild(dice2);
			dice2 = null;
		}
	}
	
}
