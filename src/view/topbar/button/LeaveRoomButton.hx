package view.topbar.button;

import govnoknopka.button.Button;
import openfl.geom.Rectangle;
import openfl.text.TextFormat;
import view.topbar.skin.BlackSkinButton;

/**
 * ...
 * @author Igor Skotnikov
 */
class LeaveRoomButton extends Button 
{

	var mediator:LeaveRoomButtonMediator;
	public function new() 
	{
		super(new BlackSkinButton(), new Rectangle(0,0,800,480), new TextFormat(GameAssets.getInstance().font.fontName, 12, 0xFFFFFF));
		buttonMode = true;
		setSize(30, 30);
		text = Localization.getInstance().data.leaveRoomButtonLabel;
		
		toolTip = createToolTip(new TextFormat(GameAssets.getInstance().font.fontName, 12, 0x222222), Localization.getInstance().data.leaveRoomButtonTooltip);
		
		mediator = new LeaveRoomButtonMediator(this);
	}

}
