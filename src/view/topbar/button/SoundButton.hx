package view.topbar.button;

import govnoknopka.button.Button;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.PixelSnapping;
import openfl.geom.Rectangle;
import openfl.text.TextFormat;
import view.topbar.skin.BlackSkinButton;

/**
 * ...
 * @author Igor Skotnikov
 */
class SoundButton extends Button 
{

	var mediator:SoundButtonMediator;
	
	var on_iconOver:Bitmap;
	var on_iconDown:Bitmap;
	var on_iconUp:Bitmap;
	
	var off_iconOver:Bitmap;
	var off_iconDown:Bitmap;
	var off_iconUp:Bitmap;
	
	public function new() 
	{
		super(new BlackSkinButton(), new Rectangle(0,0,800,480), new TextFormat(GameAssets.getInstance().font.fontName, 12, 0xFFFF00));
		buttonMode = true;
		setSize(30, 30);
		iconOver	= on_iconOver = new Bitmap(Assets.getBitmapData("img/ui/button/SoundEnabledIconOverAsset.png"), PixelSnapping.AUTO, true);
		iconDown	= on_iconDown = new Bitmap(Assets.getBitmapData("img/ui/button/SoundEnabledIconDownAsset.png"), PixelSnapping.AUTO, true);
		iconUp 		= on_iconUp = new Bitmap(Assets.getBitmapData("img/ui/button/SoundEnabledIconUpAsset.png"),	PixelSnapping.AUTO, true);
		
		off_iconOver	= new Bitmap(Assets.getBitmapData("img/ui/button/SoundDisabledIconOverAsset.png"), PixelSnapping.AUTO, true);
		off_iconDown	= new Bitmap(Assets.getBitmapData("img/ui/button/SoundDisabledIconDownAsset.png"), PixelSnapping.AUTO, true);
		off_iconUp		= new Bitmap(Assets.getBitmapData("img/ui/button/SoundDisabledIconUpAsset.png"),	PixelSnapping.AUTO, true);
		
		iconHeight	= 14;
		iconWidth	= 22;
		
		toolTip = createToolTip(new TextFormat(GameAssets.getInstance().font.fontName, 12, 0x222222), "Full Screen");
		
		mediator = new SoundButtonMediator(this);
	}
	
	public function setIcons(soundOn:Bool):Void {
		if (soundOn) {
			iconOver	= on_iconOver;
			iconDown	= on_iconDown;
			iconUp 		= on_iconUp;
		}
		else {
			iconOver	= off_iconOver;
			iconDown	= off_iconDown;
			iconUp 		= off_iconUp;
		}
		iconOver.visible = true;
		iconUp.visible = false;
		iconDown.visible = false;
		iconHeight	= 14;
		iconWidth	= 22;
	}

}
