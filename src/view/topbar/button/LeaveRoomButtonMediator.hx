package view.topbar.button;

import data.manager.popUpManager.PopUpManager;
import govnoknopka.button.Button;
import openfl.events.MouseEvent;
import view.popUpLayer.leaveRoomPopUp.LeaveRoomPopUp;

/**
 * ...
 * @author Igor Skotnikov
 */
class LeaveRoomButtonMediator
{

	var view:Button;
	public function new(view:Button) 
	{
		this.view = view;
		init();
	}
	
	inline private function init() {
		view.addEventListener(MouseEvent.MOUSE_UP, MOUSE_HANDLER);
	}
	
	private function MOUSE_HANDLER(e:MouseEvent) {
		PopUpManager.getInstance().addPopUp(new LeaveRoomPopUp());
		//GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.SENDING_PACKET, AppCommands.MULTIPLAYER_PLAYER_LEAVE_ROOM));
	}
}