package view.topbar.button;

import govnoknopka.button.Button;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.PixelSnapping;
import openfl.geom.Rectangle;
import openfl.text.TextFormat;
import view.topbar.skin.BlackSkinButton;

/**
 * ...
 * @author Igor Skotnikov
 */
class FullScreenButton extends Button 
{

	var mediator:FullScreenButtonMediator;
	public function new() 
	{
		super(new BlackSkinButton(), new Rectangle(0,0,800,480), new TextFormat(GameAssets.getInstance().font.fontName, 12, 0xFFFF00));
		buttonMode = true;
		setSize(30, 30);
		iconOver	= new Bitmap(Assets.getBitmapData("img/ui/button/FullScreenIconOverAsset.png"), PixelSnapping.AUTO, true);
		iconDown	= new Bitmap(Assets.getBitmapData("img/ui/button/FullScreenIconDownAsset.png"), PixelSnapping.AUTO, true);
		iconUp 		= new Bitmap(Assets.getBitmapData("img/ui/button/FullScreenIconUpAsset.png"),	PixelSnapping.AUTO, true);
		iconHeight	= 20;
		iconWidth	= 20;
		
		toolTip = createToolTip(new TextFormat(GameAssets.getInstance().font.fontName, 12, 0x222222), Localization.getInstance().data.screenButtonTooltip);
		
		mediator = new FullScreenButtonMediator(this);
	}

}
