package view.topbar;

import openfl.display.Sprite;
import openfl.events.Event;
import events.TopBarEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class TopBarMediator
{
	/* VARS */
	var topBar:TopBar;
	
	//-- inject --//
	private var contextview:Sprite;
	
	/* CONSTRUCTOR */
	public function new(topBar:TopBar) 
	{
		this.topBar = topBar;
		contextview = Context.getInstance().contextview;
		setupListeners();
	}
	
	private function setupListeners():Void {
		contextview.stage.addEventListener(Event.RESIZE, RESIZE_HANDLER);
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(TopBarEvent.SET_BANK, SET_BANK_HANDLER);
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(TopBarEvent.SET_BALANCE, SET_BALANCE_HANDLER);
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(TopBarEvent.SET_GAME_ID, SET_GAME_ID_HANDLER);
	}
	
	/* HANDLER */
	private function RESIZE_HANDLER(e:Event):Void {
		topBar.resize(contextview.stage.stageWidth);
	}
	
	private function SET_BANK_HANDLER(e:TopBarEvent):Void {
		topBar.animateBank(e.data.bank);
	}
	private function SET_BALANCE_HANDLER(e:TopBarEvent):Void {
		topBar.animateBalance(e.data.balance);
	}
	private function SET_GAME_ID_HANDLER(e:TopBarEvent):Void {
		topBar.setGameID(e.data.gameID);
	}
	
}