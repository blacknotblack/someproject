package view.table;

import data.mutiplayer.Room;
import data.narde.DeckModel;
import events.GameEvent;
import events.GameModelEvent;
import flash.display.Sprite;
import openfl.events.EventDispatcher;
import view.table.Deck;

/**
 * ...
 * @author Igor Skotnikov
 */
class DeckMediator
{
	/* VARS */
	private var deck:Deck;
	
	/* inject */
	private var room:Room;
	private var eventDispatcher:EventDispatcher;
	private var contextview:Sprite;
	private var deckModel:DeckModel;
	
	/* CONSTRUCTOR */
	public function new(deck:Deck) 
	{
		this.deck = deck;
		
		room = Room.getInstance();
		deckModel = DeckModel.getInstance();
		
		setupListeners();

	}
	
	/* API */
	private function setupListeners():Void {
		eventDispatcher = GlobalEventDispatcher.getInstance().eventDispatcher;
		eventDispatcher.addEventListener(GameEvent.DRAW_CHECKERS,			DRAW_CHECKERS_HANDLER);
		
		eventDispatcher.addEventListener(GameEvent.ENABLE_USER_CHECKERS,	ENABLE_USER_CHECKERS_HANDLER);
		eventDispatcher.addEventListener(GameEvent.DISABLE_USER_CHECKERS,	DISABLE_USER_CHECKERS_HANDLER);
		
		eventDispatcher.addEventListener(GameModelEvent.MOVE_CHECKER_TO_POS,	MOVE_CHECKER_TO_POS_HANDLER);
		eventDispatcher.addEventListener(GameModelEvent.ARRANGE_CHECKERS_ORDER, ARRANGE_CHECKERS_ORDER_HANDLER);
		
		eventDispatcher.addEventListener(GameEvent.REMOVE_CHECKER_FROM_DECK_ADD_TO_TRAY, REMOVE_CHECKER_FROM_DECK_ADD_TO_TRAY_HANDLER);
	}
	
	/* HANDLERS */
	private function DRAW_CHECKERS_HANDLER(e:GameEvent ):Void {
		deck.drawCheckers(e.data.mainPlayerCheckerType);
	}
	private function ENABLE_USER_CHECKERS_HANDLER(e:GameEvent):Void {
		var headPos:Int = (room.player.checkerType == 0)?0:12;
		for (i in 0...deckModel.deck.length) {
			if (i == headPos && deckModel.deck[headPos].length != deckModel.headLen && deckModel.deck[i].length < 14) continue;
			if (deckModel.deck[i].length > 0 && deckModel.deck[i][deckModel.deck[i].length - 1].type == room.player.checkerType) {
				deck.activateCheckerById(deckModel.deck[i][deckModel.deck[i].length - 1].id);
			}
		}
		
	}
	private function DISABLE_USER_CHECKERS_HANDLER(e:GameEvent):Void {
		deck.deactivateCheckers();
	}
	private function MOVE_CHECKER_TO_POS_HANDLER(e:GameEvent):Void {
		deck.moveCheckerToPos(e.data.checker);
	}
	private function ARRANGE_CHECKERS_ORDER_HANDLER(e:GameModelEvent):Void {
		deck.arangeOrderByPosition(e.data.nardeDeckCellPos, (room.player.checkerType == 1));
	}
	
	private function REMOVE_CHECKER_FROM_DECK_ADD_TO_TRAY_HANDLER(e:GameEvent):Void {
		deck.removeCheckerFromScene(e.data.id);
	}
}