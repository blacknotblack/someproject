package view.table;
import data.narde.DeckModel;
import data.narde.Narde;
import haxe.Json;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.display.PixelSnapping;
import data.narde.PositionOnTable;

/**
 * ...
 * @author Igor Skotnikov
 */
class Deck extends Sprite
{
	/* VARS */
	var image:Bitmap;
	var checkers:Array<Checker>;
	var fakeLayer:FakeLayer;
	var deckMediator:DeckMediator;
	var deckModel:DeckModel;
	
	/* CONSTRUCTOR */
	public function new() 
	{
		super();
		deckMediator = new DeckMediator(this);
		deckModel = DeckModel.getInstance();
		checkers = [];
		//alpha = 0;
		drawDeck();
		drawFakeLayer();
		//drawCheckers();
	}
	
	/* API */
	public function show():Void {
		alpha = 1;
	}
	public function hide():Void {
		alpha = 0;
	}
	
	public function getCheckerById(id:Int):Checker {
		var checker:Null<Checker> = null;
		for (i in 0...checkers.length) {
			if (checkers[i].narde.id == id) checker = checkers[i];
		}
		return checker;
	}
	public function activateCheckerById(id:Int):Void {
		for (i in 0...checkers.length) {
			if (checkers[i].narde.id == id) checkers[i].activateChecker();
		}
	}
	public function deactivateCheckers():Void {
		for (i in 0...checkers.length) {
			checkers[i].deactivateChecker();
		}
	}
	
	public function drawCheckers(mainPlayerCheckerType:Int):Void {
		for (i in 0...deckModel.deck.length) {
			for (k in 0...deckModel.deck[i].length) {
				var narde:Narde = deckModel.deck[i][k];
				var ch:Checker = new Checker(narde);
				checkers.push(ch);
				if (mainPlayerCheckerType == 0) {
					if (narde.pos < 12) {
						ch.row = k;
						ch.col = 11-narde.pos;
					}
					else {
						ch.row = 17 - k;
						ch.col = narde.pos - 12;
					}
					ch.x = PositionOnTable.position[ch.row][ch.col].x;
					ch.y = PositionOnTable.position[ch.row][ch.col].y;
					ch.scaleX = ch.scaleY = Math.pow(0.97, 17-ch.row);
				}
				else {
					if (narde.pos > 11) {
						ch.row = k;
						ch.col = 23 - narde.pos;
					}
					else {
						ch.row = 17 - k;
						ch.col = narde.pos;
					}
					ch.x = PositionOnTable.position[ch.row][ch.col].x;
					ch.y = PositionOnTable.position[ch.row][ch.col].y;
					ch.scaleX = ch.scaleY = Math.pow(0.97, 17-ch.row);
				}			
				addChild(ch);
			}
		}
		//for (i in 0...15) {														//white checkers
			//checkers[i] = new Checker({id:i, pos:0, type:0});
			//// work with Possition Class 
			//if (mainPlayerCheckerType == 0) {									//main is white
				//checkers[i].x = PositionOnTable.position[i][11].x;
				//checkers[i].y = PositionOnTable.position[i][11].y;
				//checkers[i].row = i;
				//checkers[i].col = 11;
				//checkers[i].scaleX = checkers[i].scaleY = Math.pow(0.97, 17-i);
			//}
			//else {																//main is black
				//checkers[i].x = PositionOnTable.position[17-i][0].x;
				//checkers[i].y = PositionOnTable.position[17-i][0].y;
				//checkers[i].row = 17-i;
				//checkers[i].col = 0;
				//checkers[i].scaleX = checkers[i].scaleY = Math.pow(0.97, i);
			//}
			//addChild(checkers[i]);
		//}	
		//for (i in 0...15) {													//black checkers
			//checkers[i+15] = new Checker({id:i+15, pos:12, type:1});
			//if (mainPlayerCheckerType == 0) {									//main is white
				//checkers[i+15].x = PositionOnTable.position[17-i][0].x;
				//checkers[i+15].y = PositionOnTable.position[17-i][0].y;
				//checkers[i+15].row = 17-i;
				//checkers[i+15].col = 0;
				//checkers[i+15].scaleX = checkers[i+15].scaleY = Math.pow(0.97, i);
			//}
			//else {																//main is black
				//checkers[i+15].x = PositionOnTable.position[i][11].x;
				//checkers[i+15].y = PositionOnTable.position[i][11].y;
				//checkers[i+15].row = i;
				//checkers[i+15].col = 11;
				//checkers[i+15].scaleX = checkers[i+15].scaleY = Math.pow(0.97, 17-i);
			//}
			//addChild(checkers[i+15]);
		//}
		//setCheckersByHeight();
	}
	public function moveCheckerToPos(newchecker:Dynamic):Void {
		var ch:Checker = getCheckerById(newchecker.id);
		if(ch!=null) ch.moveToPos(newchecker.to);
	}
	
	public function drawFakeLayer():Void{	
		removeFakeLayer();
		
		fakeLayer = new FakeLayer();
		addChild(fakeLayer);
	}
	private function removeFakeLayer():Void {
		if(fakeLayer!=null){
			if (contains(fakeLayer)) removeChild(fakeLayer);
			fakeLayer = null;
		}
	}
	public function drawDeck():Void{	
		removeDeck();
			
		image = new Bitmap(GameAssets.getInstance().DECK, PixelSnapping.ALWAYS, true);
		//image.width = 1200;
		//image.height = 800;
		image.x = -220;
		image.y = -63;
		addChild(image);
	}
	private function removeDeck():Void {
		if(image!=null){
			if (contains(image)) removeChild(image);
			image = null;
		}
	}
	
	public function arangeOrderByPosition(pos:Int, reverse:Bool):Void {
		for (i in 0...deckModel.deck[pos].length) {
			if (pos < 12) {
				if(reverse) addChild(getCheckerById(deckModel.deck[pos][deckModel.deck[pos].length-i-1].id));
				else addChild(getCheckerById(deckModel.deck[pos][i].id));
			}
			else {
				if(reverse) addChild(getCheckerById(deckModel.deck[pos][i].id));
				else addChild(getCheckerById(deckModel.deck[pos][deckModel.deck[pos].length-i-1].id));
			}
		}
	}
	
	public function removeCheckerFromScene(id:Int):Void {
		var checker:Checker = getCheckerById(id);
		checkers.remove(checker);
		removeChild(checker);
		checker.dispose();
		checker = null;
	}
	
	
	public function dispose():Void {
		removeDeck();
	}
	
}