package view.table;
import data.narde.Narde;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.display.PixelSnapping;

/**
 * ...
 * @author Igor Skotnikov
 */
class Checker extends Sprite
{
	public var narde:Narde;
	
	public var col:Int;
	public var row:Int;
	
	private var image:Bitmap;
	private var mediator:CheckerMediator;
	
	public function new(narde:Narde)
	{
		super();
		this.narde = narde;
		mediator = new CheckerMediator(this);
		drawChecker();
	}
	
	public function activateChecker():Void {
		mediator.activateChecker();
	}
	public function deactivateChecker():Void {
		mediator.deactivateChecker();
	}
	
	public function moveToPos(pos:Int):Void {
		mediator.moveToPos(pos);
	}
	public function dispose():Void {
		mediator = null;
		removeChild(image);
		image = null;
	}
	
	private function drawChecker():Void {
		image = new Bitmap(((narde.type == 0)?GameAssets.getInstance().CHECKER_WHITE:GameAssets.getInstance().CHECKER_BLACK), PixelSnapping.ALWAYS, true);
		image.width = image.height = 60;
		image.x = -image.width / 2;
		image.y = -image.height / 2;
		addChild(image);
	}
	
}