package view.table;

import data.const.AppCommands;
import data.mutiplayer.Room;
import data.narde.DeckModel;
import data.narde.PositionOnTable;
import events.FakeLayerEvent;
import events.GameEvent;
import events.GameModelEvent;
import haxe.Json;
import openfl.events.MouseEvent;
import openfl.geom.Point;
import service.socket.SocketServiceEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class CheckerMediator
{
	/* VARS */
	private var checker:Checker;
	private var deckModel:DeckModel;
	private var room:Room;
	
	/* CONSTRUCTOR */
	public function new(checker:Checker) 
	{
		this.checker = checker;
		deckModel = DeckModel.getInstance();
		room = Room.getInstance();
	}
	
	/* API */
	public function activateChecker():Void {
		checker.addEventListener(MouseEvent.MOUSE_DOWN, MOUSE_DOWN_HANDLER);
	}
	public function deactivateChecker():Void {
		if(checker.stage.hasEventListener(MouseEvent.MOUSE_UP)) MOUSE_UP_HANDLER(new MouseEvent(MouseEvent.MOUSE_UP));
		checker.removeEventListener(MouseEvent.MOUSE_DOWN, MOUSE_DOWN_HANDLER);
	}
	
	/* HANDLERS */
	private function MOUSE_DOWN_HANDLER(e:MouseEvent):Void {
		checker.stage.addEventListener(MouseEvent.MOUSE_MOVE, 	MOUSE_MOVE_HANDLER);
		checker.stage.addEventListener(MouseEvent.MOUSE_UP, 	MOUSE_UP_HANDLER);
		deckModel.removeCheckerFromPos(checker.narde);
		checker.parent.addChild(checker);
		//trace("MOUSE_AFTER_UP_DECK_MODEL:\n" + Json.stringify(deckModel.deck));
		GlobalEventDispatcher.getInstance().dispatch(new FakeLayerEvent(FakeLayerEvent.SHOW_FAKE_CHECKER, { pos:checker.narde.pos } ));
	}
	
	private function MOUSE_MOVE_HANDLER(e:MouseEvent):Void {
		var pos:Point = checker.parent.globalToLocal(new Point(e.stageX, e.stageY));
		checker.x = pos.x;
		checker.y = pos.y;
		
		var min:Float = Math.abs(PositionOnTable.position[0][0].y - pos.y);
		var pow:Int = 17;
		for (i in 1...18) {
			var dist:Float = Math.abs(PositionOnTable.position[i][0].y - pos.y);
			if (dist < min) {
				min = dist;
				pow = 17 - i;
			}
		}
		checker.scaleX = checker.scaleY = Math.pow(0.97, pow);
	}
	private function MOUSE_UP_HANDLER(e:MouseEvent):Void {
		checker.stage.removeEventListener(MouseEvent.MOUSE_MOVE, 	MOUSE_MOVE_HANDLER);
		checker.stage.removeEventListener(MouseEvent.MOUSE_UP, 		MOUSE_UP_HANDLER);
		var pos:Point = checker.parent.globalToLocal(new Point(e.stageX, e.stageY));
		var pow:Int = 17;
		var col:Int = 0;
		var row:Int = 0;
		
		var min:Float = Math.abs(PositionOnTable.position[0][0].y - pos.y);
		for (i in 1...18) {
			var distY:Float = Math.abs(PositionOnTable.position[i][0].y - pos.y);
			if (distY < min) {
				min = distY;
				pow = 17 - i;
				row = i;
			}
		}
		var min:Float = Math.abs(PositionOnTable.position[row][0].x - pos.x);
		for (i in 1...12) {
			var distX:Float = Math.abs(PositionOnTable.position[row][i].x - pos.x);
			if (distX < min) {
				min = distX;
				col = i;
			}
		}
		
		var nardeDeckCellPos:Int = 0;								// позиция на доске	0...23
		var len:Int;												// кол-во нард в позиции
		var perhapsPositionInRow:Int = -1;							// потенциальное расположение в позиции по высоте
		
		if (deckModel.allInDom(room.player.checkerType) && col == 11) {
			var betweenNearby:Float = PositionOnTable.position[row][11].x - PositionOnTable.position[row][10].x;
			if (betweenNearby + PositionOnTable.position[row][col].x < pos.x) {
				nardeDeckCellPos = -1; 								// выкидываем в лоток
			}
		}
		if(nardeDeckCellPos != -1){
			if (room.player.checkerType == 1) nardeDeckCellPos += 12;
			if (row <= 8) {
				nardeDeckCellPos +=	11 - col;
				len = deckModel.deck[nardeDeckCellPos].length;
				perhapsPositionInRow = len;
				{ // check for 9+
					var _pos:Int = 12 + col;
					if (room.player.checkerType == 1) _pos += 12;
					if (_pos > 23) _pos -= 24;
					trace(" -> ",row, _pos, deckModel.deck[_pos].length);
					if (row == 17 - deckModel.deck[_pos].length) {
						nardeDeckCellPos = _pos;
						perhapsPositionInRow = row;
					}
				}
			}
			else {
				nardeDeckCellPos += 12 + col;
				if (nardeDeckCellPos > 23) nardeDeckCellPos -= 24;
				len = deckModel.deck[nardeDeckCellPos].length;
				perhapsPositionInRow = 17 - len;
				{ // check for 9+
					var _pos:Int = 11 - col;
					if (room.player.checkerType == 1) _pos += 12;
					trace(" -> ",row, _pos, deckModel.deck[_pos].length, nardeDeckCellPos);
					if (row == deckModel.deck[_pos].length) {
						nardeDeckCellPos = _pos;
						perhapsPositionInRow = row;
					}
				}
			}
		}
		var positionFinded:Bool = false;
		trace(Json.stringify(deckModel.availablePosition));
		for (i in 0...deckModel.availablePosition.length) {
			if (nardeDeckCellPos == deckModel.availablePosition[i].position && positionFinded == false) {
				if (Room.getInstance().player.double) {
					var count:Int = Std.int(deckModel.availablePosition[i].dicePoints / Room.getInstance().player.dices[0]);
					trace("Remove dices " + deckModel.availablePosition[i].dicePoints);
					for (k in 0...count) Room.getInstance().player.dices.shift();
				}
				else {
					trace("Remove dices " + deckModel.availablePosition[i].dicePoints);
					if (Room.getInstance().player.dices.remove(deckModel.availablePosition[i].dicePoints) == false) {
						Room.getInstance().player.dices = [];
					}
				}
				positionFinded = true;
			}
		}
		//if ((len > 0 && deckModel.deck[nardeDeckCellPos][0].type == room.player.checkerType) || len == 0) {
		if (positionFinded) {
			GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.SENDING_PACKET, AppCommands.GAME_MOVE, {"checker": {id:checker.narde.id, from:checker.narde.pos, to:nardeDeckCellPos}} ));
			if (nardeDeckCellPos == -1) {
				GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.DISABLE_USER_CHECKERS));
				GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.REMOVE_CHECKER_FROM_DECK_ADD_TO_TRAY, { id:checker.narde.id } ));
			}
			else {
				checker.x = PositionOnTable.position[perhapsPositionInRow][col].x;
				checker.y = PositionOnTable.position[perhapsPositionInRow][col].y;
				//if (checker.row != perhapsPositionInRow || checker.col != col) {
				//	GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.SENDING_PACKET, AppCommands.GAME_MOVE, {"checker": {id:checker.narde.id, from:checker.narde.pos, to:nardeDeckCellPos}} ));
					checker.narde.pos = nardeDeckCellPos;
				//}
				checker.row = perhapsPositionInRow;
				checker.col = col;
				GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.DISABLE_USER_CHECKERS));
				checker.scaleX = checker.scaleY = Math.pow(0.97, 17 - checker.row);
				deckModel.addCheckerToPos(checker.narde, nardeDeckCellPos);
				GlobalEventDispatcher.getInstance().dispatch(new GameModelEvent(GameModelEvent.ARRANGE_CHECKERS_ORDER, { nardeDeckCellPos:nardeDeckCellPos } ));
			}
			
		}
		else { // на старую позицию
			checker.x = PositionOnTable.position[checker.row][checker.col].x;
			checker.y = PositionOnTable.position[checker.row][checker.col].y;
			checker.scaleX = checker.scaleY = Math.pow(0.97, 17 - checker.row);
			deckModel.addCheckerToPos(checker.narde, checker.narde.pos);
			GlobalEventDispatcher.getInstance().dispatch(new GameModelEvent(GameModelEvent.ARRANGE_CHECKERS_ORDER, { nardeDeckCellPos:checker.narde.pos } ));
		}
		
		//checker.scaleX = checker.scaleY = Math.pow(0.97, 17 - checker.row);
		//deckModel.addCheckerToPos(checker.narde, nardeDeckCellPos);
		//GlobalEventDispatcher.getInstance().dispatch(new GameModelEvent(GameModelEvent.ARRANGE_CHECKERS_ORDER, { nardeDeckCellPos:nardeDeckCellPos } ));
		GlobalEventDispatcher.getInstance().dispatch(new FakeLayerEvent(FakeLayerEvent.HIDE_FAKE_CHECKER));
	}
	
	public function moveToPos(pos:Int):Void {
		if (pos == -1) 
		{
			deckModel.removeCheckerFromPos(checker.narde);
			GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.REMOVE_CHECKER_FROM_DECK_ADD_TO_TRAY, { id:checker.narde.id } ));
		}
		else 
		{
			var requestPos:Int = pos;
			if (room.player.checkerType == 1) {
				pos += 12;
				if (pos > 23) pos -= 24;
			}
			var col:Int;
			var row:Int;
			if (pos < 12) {
				col = 11 - pos;
				row = deckModel.deck[requestPos].length;
			}
			else {
				col = pos - 12;
				row = 17 - deckModel.deck[requestPos].length;
			}
			
			checker.x = PositionOnTable.position[row][col].x;
			checker.y = PositionOnTable.position[row][col].y;
			checker.scaleX = checker.scaleY = Math.pow(0.97, 17 - row);
			checker.row = row;
			checker.col = col;
			deckModel.moveCheckerToPos(checker.narde, requestPos);
			checker.narde.pos = requestPos;
			GlobalEventDispatcher.getInstance().dispatch(new GameModelEvent(GameModelEvent.ARRANGE_CHECKERS_ORDER, { nardeDeckCellPos:requestPos } ));
		}
		
	}
	
}