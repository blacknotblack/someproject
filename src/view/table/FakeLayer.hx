package view.table;
import openfl.display.Sprite;
import data.narde.DeckModel;

/**
 * ...
 * @author Igor Skotnikov
 */
class FakeLayer extends Sprite
{

	private var fakeLayerMediator:FakeLayerMediator;
	private var moves:Array<FakeChecker>; 
	
	public function new() 
	{
		super();
		moves = [];
		fakeLayerMediator = new FakeLayerMediator(this);
	}
	
	public function showMoves(availablePos:Array<Dynamic>):Void {
		for (i in 0...availablePos.length) {
			if (availablePos[i].position < 0) continue;
			var ch:FakeChecker = new FakeChecker(availablePos[i].position, DeckModel.getInstance().availablePosition[i].position);
			addChild(ch);
			moves.push(ch);
		}
	}
	public function removeMoves():Void {
		for (i in 0...moves.length) {
			removeChild(moves[i]);
			moves[i] = null;
		}
		moves = [];
	}
	
}