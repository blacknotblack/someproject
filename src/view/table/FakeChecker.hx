package view.table;
import data.narde.DeckModel;
import data.narde.PositionOnTable;
import openfl.display.Bitmap;
import openfl.display.PixelSnapping;
import openfl.display.Sprite;

/**
 * ...
 * @author Igor Skotnikov
 */
class FakeChecker extends Sprite
{
	private var image:Bitmap;
	private var deckModel:DeckModel;
	
	public function new(pos:Int, posInModel:Int)
	{
		super();
		deckModel = DeckModel.getInstance();
		image = new Bitmap(GameAssets.getInstance().CHECKER_FAKE, PixelSnapping.ALWAYS, true);
		image.width = image.height = 65;
		image.x = -image.width / 2;
		image.y = -image.height / 2;
		addChild(image);
		trace(pos, posInModel);
		var row:Int;
		var col:Int;
		if (pos < 12) {
			row = deckModel.deck[posInModel].length;
			col = 11 - pos;
		}
		else {
			row = 17 - deckModel.deck[posInModel].length;
			col = pos - 12;
		}
		this.x = PositionOnTable.position[row][col].x;
		this.y = PositionOnTable.position[row][col].y;
		this.scaleX = this.scaleY = Math.pow(0.97, 17 - row);
	}
}