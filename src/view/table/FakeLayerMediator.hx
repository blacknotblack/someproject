package view.table;

import data.narde.DeckModel;
import events.GameEvent;
import openfl.events.EventDispatcher;
import events.FakeLayerEvent;
import data.mutiplayer.Room;
import data.narde.Narde;

/**
 * ...
 * @author Igor Skotnikov
 */
class FakeLayerMediator
{

	/* VARS */
	private var fakeLayer:FakeLayer;
	
	/* inject */
	private var eventDispatcher:EventDispatcher;
	private var deckModel:DeckModel;
	private var dices:Array<Int>;
	private var availablePos:Array<Dynamic>;
	
	/* CONSTRUCTOR */
	public function new(fakeLayer:FakeLayer) {
		this.fakeLayer = fakeLayer;
		deckModel = DeckModel.getInstance();
		setupListeners();
	}
	
	private function setupListeners():Void {
		eventDispatcher = GlobalEventDispatcher.getInstance().eventDispatcher;
		eventDispatcher.addEventListener(FakeLayerEvent.SHOW_FAKE_CHECKER, SHOW_FAKE_CHECKER_HANDLER);
		eventDispatcher.addEventListener(FakeLayerEvent.HIDE_FAKE_CHECKER, HIDE_FAKE_CHECKER_HANDLER);
	}
	
	/* HANDLERS */
	private function SHOW_FAKE_CHECKER_HANDLER(e:FakeLayerEvent):Void {
		calculateMoves(e.data.pos);
		showMoves(e.data.pos);
	}
	
	private function HIDE_FAKE_CHECKER_HANDLER(e:FakeLayerEvent):Void {
		removeMoves();
		GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.NOT_GLOW_TRAY));
	}
	
	private function showMoves(pos:Int):Void {
		deckModel.availablePosition = [];
		for (i in 0...availablePos.length) {
			var position:Int = availablePos[i].position;
			var dicePoints:Int = availablePos[i].dicePoints;
			if (availablePos[i].position > 23) position -= 24;
			if (Room.getInstance().player.checkerType == 1) {
				if (availablePos[i].position != -1){
					if (availablePos[i].position > 11) availablePos[i].position -= 12;
					else availablePos[i].position += 12;
				}
			}
			deckModel.availablePosition.push( { "position":position, "dicePoints":dicePoints } );
		}
		fakeLayer.showMoves(availablePos);
		//deckModel.availablePosition = availablePos.copy();
		//for (i in 0...availablePos.length) {	
			//if (deckModel.availablePosition[i] > 23) deckModel.availablePosition[i] -= 24;
		//}
		//if (Room.getInstance().player.checkerType == 1) {
			//for (i in 0...availablePos.length) {
				//if (availablePos[i] == -1) continue;
				//availablePos[i] -= 12;
			//}
		//}
		//fakeLayer.showMoves(availablePos);
	}
	private function removeMoves():Void {
		deckModel.availablePosition = [];
		fakeLayer.removeMoves();
	}
	
	/* UTILS */
	
	/**
	 * 
	 * @param	pos - position from which a checker was taken
	 */
	private function calculateMoves(pos:Int):Void {
		availablePos = [];
		dices = Room.getInstance().player.dices;
		if (deckModel.allInDom(Room.getInstance().player.checkerType)) {
			var edgeHomeCell:Int = 24;
			var startHomeCell:Int = 18;
			switch(Room.getInstance().player.checkerType) {				
				case 0: edgeHomeCell = 24; startHomeCell = 18;
				case 1: edgeHomeCell = 12; startHomeCell = 6;
			}
			for (i in 0...dices.length) {
				if (pos + dices[i] < edgeHomeCell) {
					var checkersInPos:Array<Narde> = deckModel.deck[pos + dices[i]];
					if (checkersInPos.length == 0 || (checkersInPos.length > 0 && checkersInPos[0].type == Room.getInstance().player.checkerType)) {
						availablePos.push({ "position":pos + dices[i], "dicePoints":dices[i] });
					}
				}
				else {
					var emptyCells:Bool = true;
					for (i in startHomeCell...pos) {
						if (deckModel.deck[i].length > 0) emptyCells = false;
					}
					if (emptyCells || (edgeHomeCell-dices[i]==pos && !emptyCells)) {
						GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.GLOW_TRAY));
						availablePos.push( { "position": -1, "dicePoints":dices[i] } );
					}
				}
			}
		}
		else {
			var longTableModel:Array<Array<Narde>> = createLongTableModel(pos);
			if (Room.getInstance().player.double) {
				switch(Room.getInstance().player.checkerType) {
					case 0:
						for (i in 0...dices.length) {
							if (pos + dices[i]*(i+1) < 24){
								var checkersInPos:Array<Narde> = longTableModel[pos + dices[i]*(i+1)];
								if (checkersInPos.length == 0 || (checkersInPos.length > 0 && checkersInPos[0].type == Room.getInstance().player.checkerType)) {
									if(sixInRaw(pos + dices[i]*(i+1))) availablePos.push({ "position":pos + dices[i]*(i+1), "dicePoints":dices[i]*(i+1) }); //availablePos.push(pos + dices[i]*(i+1));
									else break;
								}
								else break;
							}
						}
					case 1:
						for (i in 0...dices.length) {
							var posForBlack:Int = pos;
							if (pos < 12) posForBlack += 24;
							if (posForBlack + dices[i]*(i+1) < 36){
								var checkersInPos:Array<Narde> = longTableModel[pos + dices[i]*(i+1)];
								if (checkersInPos.length == 0 || (checkersInPos.length > 0 && checkersInPos[0].type == Room.getInstance().player.checkerType)) {
									if(sixInRaw(pos + dices[i]*(i+1)))  availablePos.push({ "position":pos + dices[i]*(i+1), "dicePoints":dices[i]*(i+1) }); //availablePos.push(pos + dices[i]*(i+1));
									else break;
								}
								else break;
							}
						}
				}
			}
			else {
				switch(Room.getInstance().player.checkerType) {
					case 0:
						for (i in 0...dices.length) {
							if (pos + dices[i] < 24){
								var checkersInPos:Array<Narde> = longTableModel[pos + dices[i]];
								if (checkersInPos.length == 0 || (checkersInPos.length > 0 && checkersInPos[0].type == Room.getInstance().player.checkerType)) {
									if(sixInRaw(pos + dices[i])) availablePos.push({ "position":pos + dices[i], "dicePoints":dices[i] }); // availablePos.push(pos + dices[i]);
								}
							}
						}
						if (availablePos.length > 0) {
							if (pos + dices[0]+dices[1] < 24){
								var checkersInPos:Array<Narde> = longTableModel[pos + dices[0]+dices[1]];
								if (checkersInPos.length == 0 || (checkersInPos.length > 0 && checkersInPos[0].type == Room.getInstance().player.checkerType)) {
									if(sixInRaw(pos + dices[0]+dices[1])) availablePos.push({ "position":pos + dices[0]+dices[1], "dicePoints":dices[0]+dices[1] }); //availablePos.push(pos + dices[0]+dices[1]);
								}
							}
						}
					case 1:
						for (i in 0...dices.length) {
							var posForBlack:Int = pos;
							if (pos < 12) posForBlack += 24;
							if (posForBlack + dices[i] < 36){
								var checkersInPos:Array<Narde> = longTableModel[pos + dices[i]];
								if (checkersInPos.length == 0 || (checkersInPos.length > 0 && checkersInPos[0].type == Room.getInstance().player.checkerType)) {
									if(sixInRaw(pos + dices[i])) availablePos.push({ "position":pos + dices[i], "dicePoints":dices[i] }); //availablePos.push(pos + dices[i]);
								}
							}
						}
						if (availablePos.length > 0) {
							var posForBlack:Int = pos;
							if (pos < 12) posForBlack += 24;
							if (posForBlack + dices[0]+dices[1] < 36){
								var checkersInPos:Array<Narde> = longTableModel[pos + dices[0] + dices[1]];
								if (checkersInPos.length == 0 || (checkersInPos.length > 0 && checkersInPos[0].type == Room.getInstance().player.checkerType)) {
									if(sixInRaw(pos + dices[0]+dices[1])) availablePos.push({ "position":pos + dices[0]+dices[1], "dicePoints":dices[0]+dices[1] }); //availablePos.push(pos + dices[0]+dices[1]);
								}
							}
						}
				}
			}
		}
	}
	
	/*
	private function allInDom():Bool {
		var doesAll:Bool = true;
		var type:Int = Room.getInstance().player.checkerType;
		switch(type) {
			case 0:
				for (i in 0...18) {	
					if(deckModel.deck[i].length > 0) {
						if (deckModel.deck[i][0].type == Room.getInstance().player.checkerType) doesAll = false;
					}
				}
			case 1:
				for (i in 0...6) {	
					if(deckModel.deck[i].length > 0) {
						if (deckModel.deck[i][0].type == Room.getInstance().player.checkerType) doesAll = false;
					}
				}
				for (i in 12...24) {	
					if(deckModel.deck[i].length > 0) {
						if (deckModel.deck[i][0].type == Room.getInstance().player.checkerType) doesAll = false;
					}
				}
		}
		return doesAll;
	}
	*/
	
	private function createLongTableModel(pos:Int):Array<Array<Narde>> {
		var array:Array<Array<Narde>> = deckModel.deck.copy();
		array[pos] = [];
		array[pos].push({ id:100, pos:pos, type:Room.getInstance().player.checkerType });
		for (i in 24...36) {
			array[i] = array[i - 24];
		}
		return array;
	}
	
	/**
	 * 
	 * @param	pos	 - position
	 * @return	true - if checker can be set in position
	 */
	private function sixInRaw(pos:Int):Bool {
		var checkerCanBeSetInPos:Bool = true;
		var playerChType:Int = Room.getInstance().player.checkerType;
		
		var longTableModel:Array<Array<Narde>> = createLongTableModel(pos);		
		var countSameType:Int = 0;
		var startIndex:Int = 0;
		var endIndex:Int = 0;
		switch(playerChType) {
			case 0:
				startIndex = 12;
				endIndex = 36;
			case 1:
				startIndex = 0;
				endIndex = 24;
		}
		for (i in startIndex...endIndex) {
			if (longTableModel[i].length > 0 && playerChType == longTableModel[i][0].type) countSameType++;
			else countSameType = 0;
			if (countSameType == 6) {
				checkerCanBeSetInPos = false;
				//looking for opponent checker
				for (l in i...endIndex) {
					if (longTableModel[l].length > 0 && longTableModel[l][0].type != playerChType) {
						checkerCanBeSetInPos = true;
						break;
					}
				}
			}
		}
		return checkerCanBeSetInPos;
	}
}