package;

import openfl.errors.Error;

/**
 * ...
 * @author Igor Skotnikov
 */
class Localization
{

	/* VARS */
	private static var _instance:Localization = new Localization();
	
	public var data:Dynamic;
	
	public function new() 
	{
		if (_instance != null)  throw new Error("Must be called throw getInstance()");
		else {
			data = {
				rollDiceButtonLabel1: "ROLL DICE",
				rollDiceButtonLabel2: "ROLL DICES",
				rollDiceButtonToolTip1: "Roll Dice",
				rollDiceButtonToolTip2: "Roll Dices",
				
				
				// TOP BAR
				leaveRoomButtonLabel:"Leave Room",
				bankLabel:"Bank",
				balanceLabel:"User Balance",
				gameIDLabel:"Game ID:",
				
				// Tooltips
				settingsButtonTooltip:"Settings",
				restartButtonTooltip:"Restart the Game",
				leaveRoomButtonTooltip:"Return to lobby",
				soundButtonTooltip:"Sound ON/OFF",
				screenButtonTooltip:"Fullscreen ON/OFF",
				
				//PopUp
				leaveRoomPopUpTitle:"Exit",
				leaveRoomPopUpContentText1:"If you leave the game right now you'll lose your bet. Do you realy want to leave the game?",
				leaveRoomPopUpContentText2:"Do you realy want to leave the game?",
				leaveRoomPopUpButton1:"Yes",
				leaveRoomPopUpButton2:"No"
			}
		}
	}
	
	public function parseData(params:Dynamic):Void {
		var fields:Array<String> = Reflect.fields(data);
		for (field in fields) {
			if(Reflect.hasField(params, field)){
            	Reflect.setField(data, field, Reflect.getProperty(params, field));
        	}
		}
	}
	
	public static function getInstance():Localization {	
		return _instance;
	}
}