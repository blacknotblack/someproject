package;
import data.mutiplayer.Player;
import events.GameEvent;
import data.mutiplayer.Room;

/**
 * ...
 * @author Igor Skotnikov
 */
class Test
{

	public function new() 
	{
		init();
		call();
		//var a:Float = 0;
		//for (i in 0...18) {
			//a = a-45 * Math.pow(0.97, i);
			//trace(a);
		//}
		
	}
	
	function init() {
		Room.getInstance().player = new Player();
		Room.getInstance().player.createPlayer(45, "her", "" ,0,0,0,0);
	}
	
	public function call() {
		GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.DRAW_PLAYER));	
		GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.DRAW_CHECKERS, { mainPlayerCheckerType:1 } ));
		GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.ENABLE_USER_CHECKERS));
		GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.SHOW_ROLLDICE_BUTTON));
	}
	
}