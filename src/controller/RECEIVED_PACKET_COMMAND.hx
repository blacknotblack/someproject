package controller;

import data.mutiplayer.Player;
import data.narde.DeckModel;
import events.GameEvent;
import events.GameModelEvent;
import haxe.Json;
import service.socket.SocketServiceEvent;
import service.socket.Packet;
import data.mutiplayer.Room;
import data.const.AppCommands;
import events.TopBarEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class RECEIVED_PACKET_COMMAND{

	/* VARS */
	public var userID			:UInt;
	public var command			:UInt;
	public var data				:Dynamic;
	
	//-- inject --//
	private var room			:Room;
	private var deckModel		:DeckModel;
	
	/* CONSTRUCTOR */
	public function new() 
	{
		room = Room.getInstance();
		deckModel = DeckModel.getInstance();
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(SocketServiceEvent.RECEIVED_PACKET,	RECEIVED_PACKET_HANDLER);
	}
	
	/* API */
	public function RECEIVED_PACKET_HANDLER(e:SocketServiceEvent) {
		userID 	= e.packet.userID;
		command = e.packet.command;
		data	= e.packet.data;
		
		trace("RECEVED: " + e.packet);
		if (data!=null) {
			var fields:Array<String> = Reflect.fields(data);
			for (field in fields) {
				switch(field) {
					case "table":
						if (Reflect.hasField(data.table, "active")) {
							room.setActive(data.table.active);
						}
						if (Reflect.hasField(data.table, "narde")) {
							deckModel.update(data.table.narde);
						}
						if (Reflect.hasField(data.table, "winner")) {
							room.setWinner(data.table.winner);
						}
					case "player":
						if (room.player.checkerType ==-1 && data.player.checkerType != -1) {
							GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.DRAW_CHECKERS, {mainPlayerCheckerType:data.player.checkerType}));
						}
						room.player.createPlayerByData(Reflect.field(data, field));
						if (Reflect.hasField(data.player, "balance")) {
							GlobalEventDispatcher.getInstance().dispatch(new TopBarEvent(TopBarEvent.SET_BALANCE, { "balance": data.player.balance } ));
						}
					case "opponent":
						room.opponent.createPlayerByData(Reflect.field(data, field));
					case "game":
						if (Reflect.hasField(data.game, "gameID")) {
							room.gameId = data.game.gameID;
							GlobalEventDispatcher.getInstance().dispatch(new TopBarEvent(TopBarEvent.SET_GAME_ID, { "gameID": data.game.gameID } ));
						}
						if (Reflect.hasField(data.game, "bank")) {
							GlobalEventDispatcher.getInstance().dispatch(new TopBarEvent(TopBarEvent.SET_BANK, { "bank": data.game.bank } ));
						}
				}
			}
		}
		
		switch(command) {
			case AppCommands.MULTIPLAYER_PLAYER_ADD:
				GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.DRAW_PLAYER));
				GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.SENDING_PACKET, AppCommands.MULTIPLAYER_WAITING_OPPONENT, {"roomID": AppData.getInstance().r} ));
			case AppCommands.MULTIPLAYER_WAITING_OPPONENT:
				GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.DRAW_OPPONENT));
				GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.SENDING_PACKET, AppCommands.GAME_PLAYER_IS_READY)); // загнать в попап
			case AppCommands.GAME_BOTH_PLAYERS_ARE_READY:
				GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.SHOW_ROLLDICE_BUTTON));
			case AppCommands.MULTIPLAYER_PLAYER_LEAVE_ROOM:
				trace("WANTS TO LEAVE");	
			case AppCommands.GAME_SHAKE_DICE:
				if (Reflect.hasField(data, "playerDice")) {
					room.player.dices[0] = data.playerDice;
					GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.SHOW_PLAYER_DICES));
				}
				if (Reflect.hasField(data, "opponentDice")) {
					room.opponent.dices[0] = data.opponentDice;
					GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.SHOW_OPPONENT_DICES));
				}
			case AppCommands.GAME_CONTINUE:
				room.gameStarted = true;
				if (room.getActivePlayer().id == room.player.id) {
					GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.SHOW_ROLLDICE_BUTTON));
				}
			case AppCommands.GAME_SHAKE_DICES:
				if (Reflect.hasField(data, "dices")) {
					var player:Player = room.getActivePlayer();
					if (room.getActivePlayer().id == room.player.id){
						//room.player.dice1 = data.dices[0];
						//room.player.dice2 = data.dices[1];
						room.player.dices = data.dices;
						room.player.double = false;
						deckModel.beforeMoveHeadLen(player.checkerType);
						if (data.dices[0] == data.dices[1]) {
							room.player.double = true;
							room.player.dices[2] = room.player.dices[3] = room.player.dices[0];
						}
						GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.HIDE_OPPONENT_DICES));
						GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.SHOW_PLAYER_DICES));
						GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.ENABLE_USER_CHECKERS));
					}
					else {
						//room.opponent.dice1 = data.dices[0];
						//room.opponent.dice2 = data.dices[1];
						room.opponent.dices = data.dices;
						GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.HIDE_PLAYER_DICES));
						GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.SHOW_OPPONENT_DICES));
					}
				}
			case AppCommands.GAME_MOVE:
				if (Reflect.hasField(data, "canMove")) {
					if(data.canMove) GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.ENABLE_USER_CHECKERS));
				}
				if (Reflect.hasField(data, "checker")) {
					GlobalEventDispatcher.getInstance().dispatch(new GameModelEvent(GameModelEvent.MOVE_CHECKER_TO_POS, {checker:data.checker}));
				}
			case AppCommands.TURN_MOVE:
				if (Reflect.hasField(data, "active")) {
					room.setActive(data.active);
					if (room.player.active) {
						GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.SHOW_ROLLDICE_BUTTON));
					}
					else {
						GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.DISABLE_USER_CHECKERS));
					}
				}
		}
	}
}