package controller;

import events.ApplicationEvent;
import haxe.Json;
import openfl.errors.Error;
import openfl.events.Event;
import openfl.events.IOErrorEvent;
import openfl.events.SecurityErrorEvent;
import openfl.net.URLLoader;
import openfl.net.URLRequest;

/**
 * ...
 * @author Igor Skotnikov
 */
class RESOURCE_LOAD_COMMAND{

	/* CONSTRUCTOR */
	public function new() 
	{
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(ApplicationEvent.RESOURCE_LOAD, RESOURCE_LOAD_HANDLER);
	}
	
	/* API */
	public function RESOURCE_LOAD_HANDLER(e:ApplicationEvent):Void {
		
		//loadResource("resourceConfig.json");
		loadResource(untyped __js__('getResourceConfig()'));
	}
	
	
	private function loadResource(id:String):Void {
		var url:String = id; 
		var request	:URLRequest	= new URLRequest(url);
		var loader	:URLLoader 	= new URLLoader();
		
		loader.addEventListener(IOErrorEvent.IO_ERROR, 				ERROR_HANDLER);
		loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, 	ERROR_HANDLER);
		loader.addEventListener(Event.COMPLETE, 					COMPLETE_HANDLER);
			
		try{
			loader.load(request);
		}catch(error:Error){
			ERROR_HANDLER(error);
		}
	}
	
	
	/* HANDLER */
	private function COMPLETE_HANDLER(e:Event):Void{
		var loader	:URLLoader 	= cast(e.target, URLLoader);
		var json	:Dynamic = Json.parse(loader.data);
		switch(json.id) {
			case "resourceConfig":
				AppData.getInstance().parseData(json.parameters);
			case "localization":
		}
		trace(AppData.getInstance().p);
		GlobalEventDispatcher.getInstance().dispatch(new ApplicationEvent(ApplicationEvent.SETUP_CONNECTION));
		loader.removeEventListener(Event.COMPLETE, 					COMPLETE_HANDLER);
	}
	private function ERROR_HANDLER(e:Error):Void{
		trace("RESOURCE LOADER SERVICE ERROR : " + e);
	}
}