package controller;

import service.socket.SocketServiceEvent;
import service.socket.Packet;
import data.mutiplayer.Room;

/**
 * ...
 * @author Igor Skotnikov
 */
class SENDING_PACKET_COMMAND{

	/* CONSTRUCTOR */
	public function new() 
	{
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(SocketServiceEvent.SENDING_PACKET,	SENDING_PACKET_HANDLER);
	}
	
	/* API */
	public function SENDING_PACKET_HANDLER(e:SocketServiceEvent) {
		Context.getInstance().socketService.send(Packet.createPacketByConstructor(Room.getInstance().player.id, e.command, e.data));
	}
}