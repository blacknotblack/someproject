package controller.viewCommands;
import events.ApplicationEvent;

#if !html5
import openfl.display.StageDisplayState;
#end

/**
 * ...
 * @author Igor Skotnikov
 */
class SCREEN_BUTTON_TRIGGERED_COMMAND
{
	/* CONSTRUCTOR */
	public function new() 
	{
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(ApplicationEvent.FULL_SCREEN, FULL_SCREEN_HANDLER);
	}
	
	/* API */
	private function FULL_SCREEN_HANDLER(e:ApplicationEvent) {
		#if html5
		
		untyped __js__ ("fullscreen()");
		
		#else
		
		if(e.data.fullscreenstate) 	Context.getInstance().contextview.stage.displayState = StageDisplayState.FULL_SCREEN;
		else						Context.getInstance().contextview.stage.displayState = StageDisplayState.NORMAL;
		
		
		#end
	}
	
}