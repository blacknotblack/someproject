package controller;

import events.ApplicationEvent;
import events.GameEvent;
import service.socket.old.Packet;
import service.socket.Packet;
import service.socket.SocketServiceEvent;
import data.const.AppCommands;

/**
 * ...
 * @author Igor Skotnikov
 */
class STARTUP_COMPLETE_COMMAND{

	/* CONSTRUCTOR */
	public function new() 
	{
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(ApplicationEvent.STARTUP_COMPLETE, STARTUP_COMPLETE_HANDLER);
	}
	
	/* API */
	public function STARTUP_COMPLETE_HANDLER(e:ApplicationEvent) {
		GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.SENDING_PACKET, AppCommands.MULTIPLAYER_PLAYER_ADD, { "hash" :AppData.getInstance().p } ));
	}
}