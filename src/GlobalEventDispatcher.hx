package;
import openfl.events.Event;
import openfl.events.EventDispatcher;
import openfl.errors.Error;

/**
 * ...
 * @author Igor Skotnikov
 */
class GlobalEventDispatcher
{
	private static var _instance:GlobalEventDispatcher = new GlobalEventDispatcher();
	
	public var eventDispatcher(default, null):EventDispatcher;
	
	public function new() {
		if(_instance!=null) throw new Error("Must be called throw getInstance()");
		eventDispatcher = new EventDispatcher();
	}
	
	public function dispatch(event:Event):Bool{
		if(eventDispatcher.hasEventListener(event.type))
			return eventDispatcher.dispatchEvent(event);
		return false;
	}
	
	public function initialize():Void {
		
	}
	
	public static function getInstance():GlobalEventDispatcher {
		return _instance;
	}
}