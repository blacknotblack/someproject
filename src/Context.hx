package;
import flash.display.Sprite;
#if html5
import service.socket.SocketService;
#end

/**
 * ...
 * @author Igor Skotnikov
 */
class Context
{
	private static var _instance:Context;
	public var contextview:Sprite;
	#if html5
	public var socketService:SocketService;
	#end
	public function new(view:Sprite) 
	{
		_instance = this;
		contextview = view;
	}
	
	public static function getInstance():Context {
		return _instance;
	}
}