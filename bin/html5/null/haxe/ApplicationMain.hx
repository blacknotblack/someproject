#if !macro


@:access(lime.app.Application)
@:access(lime.Assets)
@:access(openfl.display.Stage)


class ApplicationMain {
	
	
	public static var config:lime.app.Config;
	public static var preloader:openfl.display.Preloader;
	
	
	public static function create ():Void {
		
		var app = new openfl.display.Application ();
		app.create (config);
		
		var display = new NMEPreloader ();
		
		preloader = new openfl.display.Preloader (display);
		app.setPreloader (preloader);
		preloader.onComplete.add (init);
		preloader.create (config);
		
		#if (js && html5)
		var urls = [];
		var types = [];
		
		
		urls.push ("Roboto Medium");
		types.push (lime.Assets.AssetType.FONT);
		
		
		urls.push ("Roboto Regular");
		types.push (lime.Assets.AssetType.FONT);
		
		
		urls.push ("Roboto Slab Bold");
		types.push (lime.Assets.AssetType.FONT);
		
		
		urls.push ("Roboto Slab Regular");
		types.push (lime.Assets.AssetType.FONT);
		
		
		urls.push ("img/ui/button/BlackButtonDisabledAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/BlackButtonDownAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/BlackButtonOverAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/BlackButtonUpAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/FullScreenIconDisabledAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/FullScreenIconDownAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/FullScreenIconOverAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/FullScreenIconUpAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/GreenButtonDisabledAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/GreenButtonDownAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/GreenButtonOverAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/GreenButtonUpAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/RedButtonDisabledAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/RedButtonDownAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/RedButtonOverAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/RedButtonUpAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/SoundDisabledIconDownAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/SoundDisabledIconOverAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/SoundDisabledIconUpAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/SoundEnabledIconDownAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/SoundEnabledIconOverAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/button/SoundEnabledIconUpAsset.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/deck/CheckerBlack.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/deck/CheckerFake.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/deck/CheckerWhite.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/deck/Deck.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/deck/Deck2.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/deck/Dice1.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/deck/Dice2.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/deck/Dice3.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/deck/Dice4.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/deck/Dice5.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/deck/Dice6.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/popUp/content.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/popUp/contentBottom.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/popUp/contentTop.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/popUp/title.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/topbarBg/topBarBg.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/userIcon/UserIconBG.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		urls.push ("img/ui/userIcon/UserNoAvatarIcon.png");
		types.push (lime.Assets.AssetType.IMAGE);
		
		
		
		if (config.assetsPrefix != null) {
			
			for (i in 0...urls.length) {
				
				if (types[i] != lime.Assets.AssetType.FONT) {
					
					urls[i] = config.assetsPrefix + urls[i];
					
				}
				
			}
			
		}
		
		preloader.load (urls, types);
		#end
		
		var result = app.exec ();
		
		#if (sys && !nodejs && !emscripten)
		if (result != 0) {
			
			Sys.exit (result);
			
		}
		#end
		
	}
	
	
	public static function init ():Void {
		
		var loaded = 0;
		var total = 0;
		var library_onLoad = function (__) {
			
			loaded++;
			
			if (loaded == total) {
				
				start ();
				
			}
			
		}
		
		preloader = null;
		
		
		
		
		if (total == 0) {
			
			start ();
			
		}
		
	}
	
	
	public static function main () {
		
		config = {
			
			build: "1340",
			company: "Igor Skotnikov",
			file: "Narde",
			fps: 60,
			name: "Narde",
			orientation: "",
			packageName: "Top-bar-haxe4.0",
			version: "1.0.0",
			windows: [
				
				{
					antialiasing: 0,
					background: 7829367,
					borderless: false,
					depthBuffer: false,
					display: 0,
					fullscreen: false,
					hardware: true,
					height: 0,
					parameters: "{}",
					resizable: true,
					stencilBuffer: true,
					title: "Narde",
					vsync: false,
					width: 0,
					x: null,
					y: null
				},
			]
			
		};
		
		#if hxtelemetry
		var telemetry = new hxtelemetry.HxTelemetry.Config ();
		telemetry.allocations = true;
		telemetry.host = "localhost";
		telemetry.app_name = config.name;
		Reflect.setField (config, "telemetry", telemetry);
		#end
		
		#if (js && html5)
		#if (munit || utest)
		lime.system.System.embed (null, 0, 0, "777777");
		#end
		#else
		create ();
		#end
		
	}
	
	
	public static function start ():Void {
		
		var hasMain = false;
		var entryPoint = Type.resolveClass ("Main");
		
		for (methodName in Type.getClassFields (entryPoint)) {
			
			if (methodName == "main") {
				
				hasMain = true;
				break;
				
			}
			
		}
		
		lime.Assets.initialize ();
		
		if (hasMain) {
			
			Reflect.callMethod (entryPoint, Reflect.field (entryPoint, "main"), []);
			
		} else {
			
			var instance:DocumentClass = Type.createInstance (DocumentClass, []);
			
			/*if (Std.is (instance, openfl.display.DisplayObject)) {
				
				openfl.Lib.current.addChild (cast instance);
				
			}*/
			
		}
		
		#if !flash
		if (openfl.Lib.current.stage.window.fullscreen) {
			
			openfl.Lib.current.stage.dispatchEvent (new openfl.events.FullScreenEvent (openfl.events.FullScreenEvent.FULL_SCREEN, false, false, true, true));
			
		}
		
		openfl.Lib.current.stage.dispatchEvent (new openfl.events.Event (openfl.events.Event.RESIZE, false, false));
		#end
		
	}
	
	
	#if neko
	@:noCompletion @:dox(hide) public static function __init__ () {
		
		var loader = new neko.vm.Loader (untyped $loader);
		loader.addPath (haxe.io.Path.directory (#if (haxe_ver > 3.3) Sys.programPath () #else Sys.executablePath () #end));
		loader.addPath ("./");
		loader.addPath ("@executable_path/");
		
	}
	#end
	
	
}


@:build(DocumentClass.build())
@:keep class DocumentClass extends Main {}


#else


import haxe.macro.Context;
import haxe.macro.Expr;


class DocumentClass {
	
	
	macro public static function build ():Array<Field> {
		
		var classType = Context.getLocalClass ().get ();
		var searchTypes = classType;
		
		while (searchTypes.superClass != null) {
			
			if (searchTypes.pack.length == 2 && searchTypes.pack[1] == "display" && searchTypes.name == "DisplayObject") {
				
				var fields = Context.getBuildFields ();
				
				var method = macro {
					
					openfl.Lib.current.addChild (this);
					super ();
					dispatchEvent (new openfl.events.Event (openfl.events.Event.ADDED_TO_STAGE, false, false));
					
				}
				
				fields.push ({ name: "new", access: [ APublic ], kind: FFun({ args: [], expr: method, params: [], ret: macro :Void }), pos: Context.currentPos () });
				
				return fields;
				
			}
			
			searchTypes = searchTypes.superClass.t.get ();
			
		}
		
		return null;
		
	}
	
	
}


#end
