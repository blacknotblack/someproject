package;


import haxe.Timer;
import haxe.Unserializer;
import lime.app.Future;
import lime.app.Preloader;
import lime.app.Promise;
import lime.audio.AudioSource;
import lime.audio.openal.AL;
import lime.audio.AudioBuffer;
import lime.graphics.Image;
import lime.net.HTTPRequest;
import lime.system.CFFI;
import lime.text.Font;
import lime.utils.Bytes;
import lime.utils.UInt8Array;
import lime.Assets;

#if sys
import haxe.io.Path;
import sys.FileSystem;
#end

#if flash
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Loader;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.ProgressEvent;
import flash.media.Sound;
import flash.net.URLLoader;
import flash.net.URLRequest;
#end


class DefaultAssetLibrary extends AssetLibrary {
	
	
	public var className (default, null) = new Map <String, Dynamic> ();
	public var path (default, null) = new Map <String, String> ();
	public var type (default, null) = new Map <String, AssetType> ();
	
	private var lastModified:Float;
	private var timer:Timer;
	
	#if (desktop && !mac)
	private var rootPath = FileSystem.absolutePath (Path.directory (#if (haxe_ver >= 3.3) Sys.programPath () #else Sys.executablePath () #end)) + "/";
	#else
	private var rootPath = "";
	#end
	
	
	public function new () {
		
		super ();
		
		#if (openfl && !flash)
		
		openfl.text.Font.registerFont (__ASSET__OPENFL__font_roboto_medium_ttf);
		openfl.text.Font.registerFont (__ASSET__OPENFL__font_roboto_regular_ttf);
		openfl.text.Font.registerFont (__ASSET__OPENFL__font_robotoslab_bold_ttf);
		openfl.text.Font.registerFont (__ASSET__OPENFL__font_robotoslab_regular_ttf);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		#end
		
		#if flash
		
		className.set ("font/Roboto-Medium.ttf", __ASSET__font_roboto_medium_ttf);
		type.set ("font/Roboto-Medium.ttf", AssetType.FONT);
		className.set ("font/Roboto-Regular.ttf", __ASSET__font_roboto_regular_ttf);
		type.set ("font/Roboto-Regular.ttf", AssetType.FONT);
		className.set ("font/RobotoSlab-Bold.ttf", __ASSET__font_robotoslab_bold_ttf);
		type.set ("font/RobotoSlab-Bold.ttf", AssetType.FONT);
		className.set ("font/RobotoSlab-Regular.ttf", __ASSET__font_robotoslab_regular_ttf);
		type.set ("font/RobotoSlab-Regular.ttf", AssetType.FONT);
		className.set ("img/ui/button/BlackButtonDisabledAsset.png", __ASSET__img_ui_button_blackbuttondisabledasset_png);
		type.set ("img/ui/button/BlackButtonDisabledAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/BlackButtonDownAsset.png", __ASSET__img_ui_button_blackbuttondownasset_png);
		type.set ("img/ui/button/BlackButtonDownAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/BlackButtonOverAsset.png", __ASSET__img_ui_button_blackbuttonoverasset_png);
		type.set ("img/ui/button/BlackButtonOverAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/BlackButtonUpAsset.png", __ASSET__img_ui_button_blackbuttonupasset_png);
		type.set ("img/ui/button/BlackButtonUpAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/FullScreenIconDisabledAsset.png", __ASSET__img_ui_button_fullscreenicondisabledasset_png);
		type.set ("img/ui/button/FullScreenIconDisabledAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/FullScreenIconDownAsset.png", __ASSET__img_ui_button_fullscreenicondownasset_png);
		type.set ("img/ui/button/FullScreenIconDownAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/FullScreenIconOverAsset.png", __ASSET__img_ui_button_fullscreeniconoverasset_png);
		type.set ("img/ui/button/FullScreenIconOverAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/FullScreenIconUpAsset.png", __ASSET__img_ui_button_fullscreeniconupasset_png);
		type.set ("img/ui/button/FullScreenIconUpAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/GreenButtonDisabledAsset.png", __ASSET__img_ui_button_greenbuttondisabledasset_png);
		type.set ("img/ui/button/GreenButtonDisabledAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/GreenButtonDownAsset.png", __ASSET__img_ui_button_greenbuttondownasset_png);
		type.set ("img/ui/button/GreenButtonDownAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/GreenButtonOverAsset.png", __ASSET__img_ui_button_greenbuttonoverasset_png);
		type.set ("img/ui/button/GreenButtonOverAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/GreenButtonUpAsset.png", __ASSET__img_ui_button_greenbuttonupasset_png);
		type.set ("img/ui/button/GreenButtonUpAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/RedButtonDisabledAsset.png", __ASSET__img_ui_button_redbuttondisabledasset_png);
		type.set ("img/ui/button/RedButtonDisabledAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/RedButtonDownAsset.png", __ASSET__img_ui_button_redbuttondownasset_png);
		type.set ("img/ui/button/RedButtonDownAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/RedButtonOverAsset.png", __ASSET__img_ui_button_redbuttonoverasset_png);
		type.set ("img/ui/button/RedButtonOverAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/RedButtonUpAsset.png", __ASSET__img_ui_button_redbuttonupasset_png);
		type.set ("img/ui/button/RedButtonUpAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/SoundDisabledIconDownAsset.png", __ASSET__img_ui_button_sounddisabledicondownasset_png);
		type.set ("img/ui/button/SoundDisabledIconDownAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/SoundDisabledIconOverAsset.png", __ASSET__img_ui_button_sounddisablediconoverasset_png);
		type.set ("img/ui/button/SoundDisabledIconOverAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/SoundDisabledIconUpAsset.png", __ASSET__img_ui_button_sounddisablediconupasset_png);
		type.set ("img/ui/button/SoundDisabledIconUpAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/SoundEnabledIconDownAsset.png", __ASSET__img_ui_button_soundenabledicondownasset_png);
		type.set ("img/ui/button/SoundEnabledIconDownAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/SoundEnabledIconOverAsset.png", __ASSET__img_ui_button_soundenablediconoverasset_png);
		type.set ("img/ui/button/SoundEnabledIconOverAsset.png", AssetType.IMAGE);
		className.set ("img/ui/button/SoundEnabledIconUpAsset.png", __ASSET__img_ui_button_soundenablediconupasset_png);
		type.set ("img/ui/button/SoundEnabledIconUpAsset.png", AssetType.IMAGE);
		className.set ("img/ui/deck/CheckerBlack.png", __ASSET__img_ui_deck_checkerblack_png);
		type.set ("img/ui/deck/CheckerBlack.png", AssetType.IMAGE);
		className.set ("img/ui/deck/CheckerFake.png", __ASSET__img_ui_deck_checkerfake_png);
		type.set ("img/ui/deck/CheckerFake.png", AssetType.IMAGE);
		className.set ("img/ui/deck/CheckerWhite.png", __ASSET__img_ui_deck_checkerwhite_png);
		type.set ("img/ui/deck/CheckerWhite.png", AssetType.IMAGE);
		className.set ("img/ui/deck/Deck.png", __ASSET__img_ui_deck_deck_png);
		type.set ("img/ui/deck/Deck.png", AssetType.IMAGE);
		className.set ("img/ui/deck/Deck2.png", __ASSET__img_ui_deck_deck2_png);
		type.set ("img/ui/deck/Deck2.png", AssetType.IMAGE);
		className.set ("img/ui/deck/Dice1.png", __ASSET__img_ui_deck_dice1_png);
		type.set ("img/ui/deck/Dice1.png", AssetType.IMAGE);
		className.set ("img/ui/deck/Dice2.png", __ASSET__img_ui_deck_dice2_png);
		type.set ("img/ui/deck/Dice2.png", AssetType.IMAGE);
		className.set ("img/ui/deck/Dice3.png", __ASSET__img_ui_deck_dice3_png);
		type.set ("img/ui/deck/Dice3.png", AssetType.IMAGE);
		className.set ("img/ui/deck/Dice4.png", __ASSET__img_ui_deck_dice4_png);
		type.set ("img/ui/deck/Dice4.png", AssetType.IMAGE);
		className.set ("img/ui/deck/Dice5.png", __ASSET__img_ui_deck_dice5_png);
		type.set ("img/ui/deck/Dice5.png", AssetType.IMAGE);
		className.set ("img/ui/deck/Dice6.png", __ASSET__img_ui_deck_dice6_png);
		type.set ("img/ui/deck/Dice6.png", AssetType.IMAGE);
		className.set ("img/ui/popUp/content.png", __ASSET__img_ui_popup_content_png);
		type.set ("img/ui/popUp/content.png", AssetType.IMAGE);
		className.set ("img/ui/popUp/contentBottom.png", __ASSET__img_ui_popup_contentbottom_png);
		type.set ("img/ui/popUp/contentBottom.png", AssetType.IMAGE);
		className.set ("img/ui/popUp/contentTop.png", __ASSET__img_ui_popup_contenttop_png);
		type.set ("img/ui/popUp/contentTop.png", AssetType.IMAGE);
		className.set ("img/ui/popUp/title.png", __ASSET__img_ui_popup_title_png);
		type.set ("img/ui/popUp/title.png", AssetType.IMAGE);
		className.set ("img/ui/topbarBg/topBarBg.png", __ASSET__img_ui_topbarbg_topbarbg_png);
		type.set ("img/ui/topbarBg/topBarBg.png", AssetType.IMAGE);
		className.set ("img/ui/userIcon/UserIconBG.png", __ASSET__img_ui_usericon_usericonbg_png);
		type.set ("img/ui/userIcon/UserIconBG.png", AssetType.IMAGE);
		className.set ("img/ui/userIcon/UserNoAvatarIcon.png", __ASSET__img_ui_usericon_usernoavataricon_png);
		type.set ("img/ui/userIcon/UserNoAvatarIcon.png", AssetType.IMAGE);
		
		
		#elseif html5
		
		var id;
		id = "font/Roboto-Medium.ttf";
		className.set (id, __ASSET__font_roboto_medium_ttf);
		
		type.set (id, AssetType.FONT);
		id = "font/Roboto-Regular.ttf";
		className.set (id, __ASSET__font_roboto_regular_ttf);
		
		type.set (id, AssetType.FONT);
		id = "font/RobotoSlab-Bold.ttf";
		className.set (id, __ASSET__font_robotoslab_bold_ttf);
		
		type.set (id, AssetType.FONT);
		id = "font/RobotoSlab-Regular.ttf";
		className.set (id, __ASSET__font_robotoslab_regular_ttf);
		
		type.set (id, AssetType.FONT);
		id = "img/ui/button/BlackButtonDisabledAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/BlackButtonDownAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/BlackButtonOverAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/BlackButtonUpAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/FullScreenIconDisabledAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/FullScreenIconDownAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/FullScreenIconOverAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/FullScreenIconUpAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/GreenButtonDisabledAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/GreenButtonDownAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/GreenButtonOverAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/GreenButtonUpAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/RedButtonDisabledAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/RedButtonDownAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/RedButtonOverAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/RedButtonUpAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/SoundDisabledIconDownAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/SoundDisabledIconOverAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/SoundDisabledIconUpAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/SoundEnabledIconDownAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/SoundEnabledIconOverAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/button/SoundEnabledIconUpAsset.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/deck/CheckerBlack.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/deck/CheckerFake.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/deck/CheckerWhite.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/deck/Deck.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/deck/Deck2.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/deck/Dice1.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/deck/Dice2.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/deck/Dice3.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/deck/Dice4.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/deck/Dice5.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/deck/Dice6.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/popUp/content.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/popUp/contentBottom.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/popUp/contentTop.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/popUp/title.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/topbarBg/topBarBg.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/userIcon/UserIconBG.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "img/ui/userIcon/UserNoAvatarIcon.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		
		
		var assetsPrefix = null;
		if (ApplicationMain.config != null && Reflect.hasField (ApplicationMain.config, "assetsPrefix")) {
			assetsPrefix = ApplicationMain.config.assetsPrefix;
		}
		if (assetsPrefix != null) {
			for (k in path.keys()) {
				path.set(k, assetsPrefix + path[k]);
			}
		}
		
		#else
		
		#if (windows || mac || linux)
		
		var useManifest = false;
		
		className.set ("font/Roboto-Medium.ttf", __ASSET__font_roboto_medium_ttf);
		type.set ("font/Roboto-Medium.ttf", AssetType.FONT);
		
		className.set ("font/Roboto-Regular.ttf", __ASSET__font_roboto_regular_ttf);
		type.set ("font/Roboto-Regular.ttf", AssetType.FONT);
		
		className.set ("font/RobotoSlab-Bold.ttf", __ASSET__font_robotoslab_bold_ttf);
		type.set ("font/RobotoSlab-Bold.ttf", AssetType.FONT);
		
		className.set ("font/RobotoSlab-Regular.ttf", __ASSET__font_robotoslab_regular_ttf);
		type.set ("font/RobotoSlab-Regular.ttf", AssetType.FONT);
		
		className.set ("img/ui/button/BlackButtonDisabledAsset.png", __ASSET__img_ui_button_blackbuttondisabledasset_png);
		type.set ("img/ui/button/BlackButtonDisabledAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/BlackButtonDownAsset.png", __ASSET__img_ui_button_blackbuttondownasset_png);
		type.set ("img/ui/button/BlackButtonDownAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/BlackButtonOverAsset.png", __ASSET__img_ui_button_blackbuttonoverasset_png);
		type.set ("img/ui/button/BlackButtonOverAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/BlackButtonUpAsset.png", __ASSET__img_ui_button_blackbuttonupasset_png);
		type.set ("img/ui/button/BlackButtonUpAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/FullScreenIconDisabledAsset.png", __ASSET__img_ui_button_fullscreenicondisabledasset_png);
		type.set ("img/ui/button/FullScreenIconDisabledAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/FullScreenIconDownAsset.png", __ASSET__img_ui_button_fullscreenicondownasset_png);
		type.set ("img/ui/button/FullScreenIconDownAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/FullScreenIconOverAsset.png", __ASSET__img_ui_button_fullscreeniconoverasset_png);
		type.set ("img/ui/button/FullScreenIconOverAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/FullScreenIconUpAsset.png", __ASSET__img_ui_button_fullscreeniconupasset_png);
		type.set ("img/ui/button/FullScreenIconUpAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/GreenButtonDisabledAsset.png", __ASSET__img_ui_button_greenbuttondisabledasset_png);
		type.set ("img/ui/button/GreenButtonDisabledAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/GreenButtonDownAsset.png", __ASSET__img_ui_button_greenbuttondownasset_png);
		type.set ("img/ui/button/GreenButtonDownAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/GreenButtonOverAsset.png", __ASSET__img_ui_button_greenbuttonoverasset_png);
		type.set ("img/ui/button/GreenButtonOverAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/GreenButtonUpAsset.png", __ASSET__img_ui_button_greenbuttonupasset_png);
		type.set ("img/ui/button/GreenButtonUpAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/RedButtonDisabledAsset.png", __ASSET__img_ui_button_redbuttondisabledasset_png);
		type.set ("img/ui/button/RedButtonDisabledAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/RedButtonDownAsset.png", __ASSET__img_ui_button_redbuttondownasset_png);
		type.set ("img/ui/button/RedButtonDownAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/RedButtonOverAsset.png", __ASSET__img_ui_button_redbuttonoverasset_png);
		type.set ("img/ui/button/RedButtonOverAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/RedButtonUpAsset.png", __ASSET__img_ui_button_redbuttonupasset_png);
		type.set ("img/ui/button/RedButtonUpAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/SoundDisabledIconDownAsset.png", __ASSET__img_ui_button_sounddisabledicondownasset_png);
		type.set ("img/ui/button/SoundDisabledIconDownAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/SoundDisabledIconOverAsset.png", __ASSET__img_ui_button_sounddisablediconoverasset_png);
		type.set ("img/ui/button/SoundDisabledIconOverAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/SoundDisabledIconUpAsset.png", __ASSET__img_ui_button_sounddisablediconupasset_png);
		type.set ("img/ui/button/SoundDisabledIconUpAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/SoundEnabledIconDownAsset.png", __ASSET__img_ui_button_soundenabledicondownasset_png);
		type.set ("img/ui/button/SoundEnabledIconDownAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/SoundEnabledIconOverAsset.png", __ASSET__img_ui_button_soundenablediconoverasset_png);
		type.set ("img/ui/button/SoundEnabledIconOverAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/button/SoundEnabledIconUpAsset.png", __ASSET__img_ui_button_soundenablediconupasset_png);
		type.set ("img/ui/button/SoundEnabledIconUpAsset.png", AssetType.IMAGE);
		
		className.set ("img/ui/deck/CheckerBlack.png", __ASSET__img_ui_deck_checkerblack_png);
		type.set ("img/ui/deck/CheckerBlack.png", AssetType.IMAGE);
		
		className.set ("img/ui/deck/CheckerFake.png", __ASSET__img_ui_deck_checkerfake_png);
		type.set ("img/ui/deck/CheckerFake.png", AssetType.IMAGE);
		
		className.set ("img/ui/deck/CheckerWhite.png", __ASSET__img_ui_deck_checkerwhite_png);
		type.set ("img/ui/deck/CheckerWhite.png", AssetType.IMAGE);
		
		className.set ("img/ui/deck/Deck.png", __ASSET__img_ui_deck_deck_png);
		type.set ("img/ui/deck/Deck.png", AssetType.IMAGE);
		
		className.set ("img/ui/deck/Deck2.png", __ASSET__img_ui_deck_deck2_png);
		type.set ("img/ui/deck/Deck2.png", AssetType.IMAGE);
		
		className.set ("img/ui/deck/Dice1.png", __ASSET__img_ui_deck_dice1_png);
		type.set ("img/ui/deck/Dice1.png", AssetType.IMAGE);
		
		className.set ("img/ui/deck/Dice2.png", __ASSET__img_ui_deck_dice2_png);
		type.set ("img/ui/deck/Dice2.png", AssetType.IMAGE);
		
		className.set ("img/ui/deck/Dice3.png", __ASSET__img_ui_deck_dice3_png);
		type.set ("img/ui/deck/Dice3.png", AssetType.IMAGE);
		
		className.set ("img/ui/deck/Dice4.png", __ASSET__img_ui_deck_dice4_png);
		type.set ("img/ui/deck/Dice4.png", AssetType.IMAGE);
		
		className.set ("img/ui/deck/Dice5.png", __ASSET__img_ui_deck_dice5_png);
		type.set ("img/ui/deck/Dice5.png", AssetType.IMAGE);
		
		className.set ("img/ui/deck/Dice6.png", __ASSET__img_ui_deck_dice6_png);
		type.set ("img/ui/deck/Dice6.png", AssetType.IMAGE);
		
		className.set ("img/ui/popUp/content.png", __ASSET__img_ui_popup_content_png);
		type.set ("img/ui/popUp/content.png", AssetType.IMAGE);
		
		className.set ("img/ui/popUp/contentBottom.png", __ASSET__img_ui_popup_contentbottom_png);
		type.set ("img/ui/popUp/contentBottom.png", AssetType.IMAGE);
		
		className.set ("img/ui/popUp/contentTop.png", __ASSET__img_ui_popup_contenttop_png);
		type.set ("img/ui/popUp/contentTop.png", AssetType.IMAGE);
		
		className.set ("img/ui/popUp/title.png", __ASSET__img_ui_popup_title_png);
		type.set ("img/ui/popUp/title.png", AssetType.IMAGE);
		
		className.set ("img/ui/topbarBg/topBarBg.png", __ASSET__img_ui_topbarbg_topbarbg_png);
		type.set ("img/ui/topbarBg/topBarBg.png", AssetType.IMAGE);
		
		className.set ("img/ui/userIcon/UserIconBG.png", __ASSET__img_ui_usericon_usericonbg_png);
		type.set ("img/ui/userIcon/UserIconBG.png", AssetType.IMAGE);
		
		className.set ("img/ui/userIcon/UserNoAvatarIcon.png", __ASSET__img_ui_usericon_usernoavataricon_png);
		type.set ("img/ui/userIcon/UserNoAvatarIcon.png", AssetType.IMAGE);
		
		
		if (useManifest) {
			
			loadManifest ();
			
			if (Sys.args ().indexOf ("-livereload") > -1) {
				
				var path = FileSystem.fullPath ("manifest");
				lastModified = FileSystem.stat (path).mtime.getTime ();
				
				timer = new Timer (2000);
				timer.run = function () {
					
					var modified = FileSystem.stat (path).mtime.getTime ();
					
					if (modified > lastModified) {
						
						lastModified = modified;
						loadManifest ();
						
						onChange.dispatch ();
						
					}
					
				}
				
			}
			
		}
		
		#else
		
		loadManifest ();
		
		#end
		#end
		
	}
	
	
	public override function exists (id:String, type:String):Bool {
		
		var requestedType = type != null ? cast (type, AssetType) : null;
		var assetType = this.type.get (id);
		
		if (assetType != null) {
			
			if (assetType == requestedType || ((requestedType == SOUND || requestedType == MUSIC) && (assetType == MUSIC || assetType == SOUND))) {
				
				return true;
				
			}
			
			#if flash
			
			if (requestedType == BINARY && (assetType == BINARY || assetType == TEXT || assetType == IMAGE)) {
				
				return true;
				
			} else if (requestedType == TEXT && assetType == BINARY) {
				
				return true;
				
			} else if (requestedType == null || path.exists (id)) {
				
				return true;
				
			}
			
			#else
			
			if (requestedType == BINARY || requestedType == null || (assetType == BINARY && requestedType == TEXT)) {
				
				return true;
				
			}
			
			#end
			
		}
		
		return false;
		
	}
	
	
	public override function getAudioBuffer (id:String):AudioBuffer {
		
		#if flash
		
		var buffer = new AudioBuffer ();
		buffer.src = cast (Type.createInstance (className.get (id), []), Sound);
		return buffer;
		
		#elseif html5
		
		return null;
		//return new Sound (new URLRequest (path.get (id)));
		
		#else
		
		if (className.exists(id)) return AudioBuffer.fromBytes (cast (Type.createInstance (className.get (id), []), Bytes));
		else return AudioBuffer.fromFile (rootPath + path.get (id));
		
		#end
		
	}
	
	
	public override function getBytes (id:String):Bytes {
		
		#if flash
		
		switch (type.get (id)) {
			
			case TEXT, BINARY:
				
				return Bytes.ofData (cast (Type.createInstance (className.get (id), []), flash.utils.ByteArray));
			
			case IMAGE:
				
				var bitmapData = cast (Type.createInstance (className.get (id), []), BitmapData);
				return Bytes.ofData (bitmapData.getPixels (bitmapData.rect));
			
			default:
				
				return null;
			
		}
		
		return cast (Type.createInstance (className.get (id), []), Bytes);
		
		#elseif html5
		
		var loader = Preloader.loaders.get (path.get (id));
		
		if (loader == null) {
			
			return null;
			
		}
		
		var bytes = loader.bytes;
		
		if (bytes != null) {
			
			return bytes;
			
		} else {
			
			return null;
		}
		
		#else
		
		if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), Bytes);
		else return Bytes.readFile (rootPath + path.get (id));
		
		#end
		
	}
	
	
	public override function getFont (id:String):Font {
		
		#if flash
		
		var src = Type.createInstance (className.get (id), []);
		
		var font = new Font (src.fontName);
		font.src = src;
		return font;
		
		#elseif html5
		
		return cast (Type.createInstance (className.get (id), []), Font);
		
		#else
		
		if (className.exists (id)) {
			
			var fontClass = className.get (id);
			return cast (Type.createInstance (fontClass, []), Font);
			
		} else {
			
			return Font.fromFile (rootPath + path.get (id));
			
		}
		
		#end
		
	}
	
	
	public override function getImage (id:String):Image {
		
		#if flash
		
		return Image.fromBitmapData (cast (Type.createInstance (className.get (id), []), BitmapData));
		
		#elseif html5
		
		return Image.fromImageElement (Preloader.images.get (path.get (id)));
		
		#else
		
		if (className.exists (id)) {
			
			var fontClass = className.get (id);
			return cast (Type.createInstance (fontClass, []), Image);
			
		} else {
			
			return Image.fromFile (rootPath + path.get (id));
			
		}
		
		#end
		
	}
	
	
	/*public override function getMusic (id:String):Dynamic {
		
		#if flash
		
		return cast (Type.createInstance (className.get (id), []), Sound);
		
		#elseif openfl_html5
		
		//var sound = new Sound ();
		//sound.__buffer = true;
		//sound.load (new URLRequest (path.get (id)));
		//return sound;
		return null;
		
		#elseif html5
		
		return null;
		//return new Sound (new URLRequest (path.get (id)));
		
		#else
		
		return null;
		//if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), Sound);
		//else return new Sound (new URLRequest (path.get (id)), null, true);
		
		#end
		
	}*/
	
	
	public override function getPath (id:String):String {
		
		//#if ios
		
		//return SystemPath.applicationDirectory + "/assets/" + path.get (id);
		
		//#else
		
		return path.get (id);
		
		//#end
		
	}
	
	
	public override function getText (id:String):String {
		
		#if html5
		
		var loader = Preloader.loaders.get (path.get (id));
		
		if (loader == null) {
			
			return null;
			
		}
		
		var bytes = loader.bytes;
		
		if (bytes != null) {
			
			return bytes.getString (0, bytes.length);
			
		} else {
			
			return null;
		}
		
		#else
		
		var bytes = getBytes (id);
		
		if (bytes == null) {
			
			return null;
			
		} else {
			
			return bytes.getString (0, bytes.length);
			
		}
		
		#end
		
	}
	
	
	public override function isLocal (id:String, type:String):Bool {
		
		var requestedType = type != null ? cast (type, AssetType) : null;
		
		#if flash
		
		//if (requestedType != AssetType.MUSIC && requestedType != AssetType.SOUND) {
			
			return className.exists (id);
			
		//}
		
		#end
		
		return true;
		
	}
	
	
	public override function list (type:String):Array<String> {
		
		var requestedType = type != null ? cast (type, AssetType) : null;
		var items = [];
		
		for (id in this.type.keys ()) {
			
			if (requestedType == null || exists (id, type)) {
				
				items.push (id);
				
			}
			
		}
		
		return items;
		
	}
	
	
	public override function loadAudioBuffer (id:String):Future<AudioBuffer> {
		
		var promise = new Promise<AudioBuffer> ();
		
		#if (flash)
		
		if (path.exists (id)) {
			
			var soundLoader = new Sound ();
			soundLoader.addEventListener (Event.COMPLETE, function (event) {
				
				var audioBuffer:AudioBuffer = new AudioBuffer();
				audioBuffer.src = event.currentTarget;
				promise.complete (audioBuffer);
				
			});
			soundLoader.addEventListener (ProgressEvent.PROGRESS, function (event) {
				
				if (event.bytesTotal == 0) {
					
					promise.progress (0);
					
				} else {
					
					promise.progress (event.bytesLoaded / event.bytesTotal);
					
				}
				
			});
			soundLoader.addEventListener (IOErrorEvent.IO_ERROR, promise.error);
			soundLoader.load (new URLRequest (path.get (id)));
			
		} else {
			
			promise.complete (getAudioBuffer (id));
			
		}
		
		#else
		
		promise.completeWith (new Future<AudioBuffer> (function () return getAudioBuffer (id)));
		
		#end
		
		return promise.future;
		
	}
	
	
	public override function loadBytes (id:String):Future<Bytes> {
		
		var promise = new Promise<Bytes> ();
		
		#if flash
		
		if (path.exists (id)) {
			
			var loader = new URLLoader ();
			loader.dataFormat = flash.net.URLLoaderDataFormat.BINARY;
			loader.addEventListener (Event.COMPLETE, function (event:Event) {
				
				var bytes = Bytes.ofData (event.currentTarget.data);
				promise.complete (bytes);
				
			});
			loader.addEventListener (ProgressEvent.PROGRESS, function (event) {
				
				if (event.bytesTotal == 0) {
					
					promise.progress (0);
					
				} else {
					
					promise.progress (event.bytesLoaded / event.bytesTotal);
					
				}
				
			});
			loader.addEventListener (IOErrorEvent.IO_ERROR, promise.error);
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			promise.complete (getBytes (id));
			
		}
		
		#elseif html5
		
		if (path.exists (id)) {
			
			var request = new HTTPRequest ();
			promise.completeWith (request.load (path.get (id) + "?" + Assets.cache.version));
			
		} else {
			
			promise.complete (getBytes (id));
			
		}
		
		#else
		
		promise.completeWith (new Future<Bytes> (function () return getBytes (id)));
		
		#end
		
		return promise.future;
		
	}
	
	
	public override function loadImage (id:String):Future<Image> {
		
		var promise = new Promise<Image> ();
		
		#if flash
		
		if (path.exists (id)) {
			
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event:Event) {
				
				var bitmapData = cast (event.currentTarget.content, Bitmap).bitmapData;
				promise.complete (Image.fromBitmapData (bitmapData));
				
			});
			loader.contentLoaderInfo.addEventListener (ProgressEvent.PROGRESS, function (event) {
				
				if (event.bytesTotal == 0) {
					
					promise.progress (0);
					
				} else {
					
					promise.progress (event.bytesLoaded / event.bytesTotal);
					
				}
				
			});
			loader.contentLoaderInfo.addEventListener (IOErrorEvent.IO_ERROR, promise.error);
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			promise.complete (getImage (id));
			
		}
		
		#elseif html5
		
		if (path.exists (id)) {
			
			var image = new js.html.Image ();
			image.onload = function (_):Void {
				
				promise.complete (Image.fromImageElement (image));
				
			}
			image.onerror = promise.error;
			image.src = path.get (id) + "?" + Assets.cache.version;
			
		} else {
			
			promise.complete (getImage (id));
			
		}
		
		#else
		
		promise.completeWith (new Future<Image> (function () return getImage (id)));
		
		#end
		
		return promise.future;
		
	}
	
	
	#if (!flash && !html5)
	private function loadManifest ():Void {
		
		try {
			
			#if blackberry
			var bytes = Bytes.readFile ("app/native/manifest");
			#elseif tizen
			var bytes = Bytes.readFile ("../res/manifest");
			#elseif emscripten
			var bytes = Bytes.readFile ("assets/manifest");
			#elseif (mac && java)
			var bytes = Bytes.readFile ("../Resources/manifest");
			#elseif (ios || tvos)
			var bytes = Bytes.readFile ("assets/manifest");
			#else
			var bytes = Bytes.readFile ("manifest");
			#end
			
			if (bytes != null) {
				
				if (bytes.length > 0) {
					
					var data = bytes.getString (0, bytes.length);
					
					if (data != null && data.length > 0) {
						
						var manifest:Array<Dynamic> = Unserializer.run (data);
						
						for (asset in manifest) {
							
							if (!className.exists (asset.id)) {
								
								#if (ios || tvos)
								path.set (asset.id, "assets/" + asset.path);
								#else
								path.set (asset.id, asset.path);
								#end
								type.set (asset.id, cast (asset.type, AssetType));
								
							}
							
						}
						
					}
					
				}
				
			} else {
				
				trace ("Warning: Could not load asset manifest (bytes was null)");
				
			}
		
		} catch (e:Dynamic) {
			
			trace ('Warning: Could not load asset manifest (${e})');
			
		}
		
	}
	#end
	
	
	public override function loadText (id:String):Future<String> {
		
		var promise = new Promise<String> ();
		
		#if html5
		
		if (path.exists (id)) {
			
			var request = new HTTPRequest ();
			var future = request.load (path.get (id) + "?" + Assets.cache.version);
			future.onProgress (function (progress) promise.progress (progress));
			future.onError (function (msg) promise.error (msg));
			future.onComplete (function (bytes) promise.complete (bytes.getString (0, bytes.length)));
			
		} else {
			
			promise.complete (getText (id));
			
		}
		
		#else
		
		promise.completeWith (loadBytes (id).then (function (bytes) {
			
			return new Future<String> (function () {
				
				if (bytes == null) {
					
					return null;
					
				} else {
					
					return bytes.getString (0, bytes.length);
					
				}
				
			});
			
		}));
		
		#end
		
		return promise.future;
		
	}
	
	
}


#if !display
#if flash

@:keep @:bind #if display private #end class __ASSET__font_roboto_medium_ttf extends null { }
@:keep @:bind #if display private #end class __ASSET__font_roboto_regular_ttf extends null { }
@:keep @:bind #if display private #end class __ASSET__font_robotoslab_bold_ttf extends null { }
@:keep @:bind #if display private #end class __ASSET__font_robotoslab_regular_ttf extends null { }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_blackbuttondisabledasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_blackbuttondownasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_blackbuttonoverasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_blackbuttonupasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_fullscreenicondisabledasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_fullscreenicondownasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_fullscreeniconoverasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_fullscreeniconupasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_greenbuttondisabledasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_greenbuttondownasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_greenbuttonoverasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_greenbuttonupasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_redbuttondisabledasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_redbuttondownasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_redbuttonoverasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_redbuttonupasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_sounddisabledicondownasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_sounddisablediconoverasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_sounddisablediconupasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_soundenabledicondownasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_soundenablediconoverasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_button_soundenablediconupasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_deck_checkerblack_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_deck_checkerfake_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_deck_checkerwhite_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_deck_deck_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_deck_deck2_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_deck_dice1_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_deck_dice2_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_deck_dice3_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_deck_dice4_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_deck_dice5_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_deck_dice6_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_popup_content_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_popup_contentbottom_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_popup_contenttop_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_popup_title_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_topbarbg_topbarbg_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_usericon_usericonbg_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__img_ui_usericon_usernoavataricon_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }


#elseif html5

@:keep #if display private #end class __ASSET__font_roboto_medium_ttf extends lime.text.Font { public function new () { super (); name = "Roboto Medium"; } } 
@:keep #if display private #end class __ASSET__font_roboto_regular_ttf extends lime.text.Font { public function new () { super (); name = "Roboto Regular"; } } 
@:keep #if display private #end class __ASSET__font_robotoslab_bold_ttf extends lime.text.Font { public function new () { super (); name = "Roboto Slab Bold"; } } 
@:keep #if display private #end class __ASSET__font_robotoslab_regular_ttf extends lime.text.Font { public function new () { super (); name = "Roboto Slab Regular"; } } 










































#else



#if (windows || mac || linux || cpp)


@:font("assets/font/Roboto-Medium.ttf") #if display private #end class __ASSET__font_roboto_medium_ttf extends lime.text.Font {}
@:font("assets/font/Roboto-Regular.ttf") #if display private #end class __ASSET__font_roboto_regular_ttf extends lime.text.Font {}
@:font("assets/font/RobotoSlab-Bold.ttf") #if display private #end class __ASSET__font_robotoslab_bold_ttf extends lime.text.Font {}
@:font("assets/font/RobotoSlab-Regular.ttf") #if display private #end class __ASSET__font_robotoslab_regular_ttf extends lime.text.Font {}
@:image("assets/img/ui/button/BlackButtonDisabledAsset.png") #if display private #end class __ASSET__img_ui_button_blackbuttondisabledasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/BlackButtonDownAsset.png") #if display private #end class __ASSET__img_ui_button_blackbuttondownasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/BlackButtonOverAsset.png") #if display private #end class __ASSET__img_ui_button_blackbuttonoverasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/BlackButtonUpAsset.png") #if display private #end class __ASSET__img_ui_button_blackbuttonupasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/FullScreenIconDisabledAsset.png") #if display private #end class __ASSET__img_ui_button_fullscreenicondisabledasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/FullScreenIconDownAsset.png") #if display private #end class __ASSET__img_ui_button_fullscreenicondownasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/FullScreenIconOverAsset.png") #if display private #end class __ASSET__img_ui_button_fullscreeniconoverasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/FullScreenIconUpAsset.png") #if display private #end class __ASSET__img_ui_button_fullscreeniconupasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/GreenButtonDisabledAsset.png") #if display private #end class __ASSET__img_ui_button_greenbuttondisabledasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/GreenButtonDownAsset.png") #if display private #end class __ASSET__img_ui_button_greenbuttondownasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/GreenButtonOverAsset.png") #if display private #end class __ASSET__img_ui_button_greenbuttonoverasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/GreenButtonUpAsset.png") #if display private #end class __ASSET__img_ui_button_greenbuttonupasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/RedButtonDisabledAsset.png") #if display private #end class __ASSET__img_ui_button_redbuttondisabledasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/RedButtonDownAsset.png") #if display private #end class __ASSET__img_ui_button_redbuttondownasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/RedButtonOverAsset.png") #if display private #end class __ASSET__img_ui_button_redbuttonoverasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/RedButtonUpAsset.png") #if display private #end class __ASSET__img_ui_button_redbuttonupasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/SoundDisabledIconDownAsset.png") #if display private #end class __ASSET__img_ui_button_sounddisabledicondownasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/SoundDisabledIconOverAsset.png") #if display private #end class __ASSET__img_ui_button_sounddisablediconoverasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/SoundDisabledIconUpAsset.png") #if display private #end class __ASSET__img_ui_button_sounddisablediconupasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/SoundEnabledIconDownAsset.png") #if display private #end class __ASSET__img_ui_button_soundenabledicondownasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/SoundEnabledIconOverAsset.png") #if display private #end class __ASSET__img_ui_button_soundenablediconoverasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/button/SoundEnabledIconUpAsset.png") #if display private #end class __ASSET__img_ui_button_soundenablediconupasset_png extends lime.graphics.Image {}
@:image("assets/img/ui/deck/CheckerBlack.png") #if display private #end class __ASSET__img_ui_deck_checkerblack_png extends lime.graphics.Image {}
@:image("assets/img/ui/deck/CheckerFake.png") #if display private #end class __ASSET__img_ui_deck_checkerfake_png extends lime.graphics.Image {}
@:image("assets/img/ui/deck/CheckerWhite.png") #if display private #end class __ASSET__img_ui_deck_checkerwhite_png extends lime.graphics.Image {}
@:image("assets/img/ui/deck/Deck.png") #if display private #end class __ASSET__img_ui_deck_deck_png extends lime.graphics.Image {}
@:image("assets/img/ui/deck/Deck2.png") #if display private #end class __ASSET__img_ui_deck_deck2_png extends lime.graphics.Image {}
@:image("assets/img/ui/deck/Dice1.png") #if display private #end class __ASSET__img_ui_deck_dice1_png extends lime.graphics.Image {}
@:image("assets/img/ui/deck/Dice2.png") #if display private #end class __ASSET__img_ui_deck_dice2_png extends lime.graphics.Image {}
@:image("assets/img/ui/deck/Dice3.png") #if display private #end class __ASSET__img_ui_deck_dice3_png extends lime.graphics.Image {}
@:image("assets/img/ui/deck/Dice4.png") #if display private #end class __ASSET__img_ui_deck_dice4_png extends lime.graphics.Image {}
@:image("assets/img/ui/deck/Dice5.png") #if display private #end class __ASSET__img_ui_deck_dice5_png extends lime.graphics.Image {}
@:image("assets/img/ui/deck/Dice6.png") #if display private #end class __ASSET__img_ui_deck_dice6_png extends lime.graphics.Image {}
@:image("assets/img/ui/popUp/content.png") #if display private #end class __ASSET__img_ui_popup_content_png extends lime.graphics.Image {}
@:image("assets/img/ui/popUp/contentBottom.png") #if display private #end class __ASSET__img_ui_popup_contentbottom_png extends lime.graphics.Image {}
@:image("assets/img/ui/popUp/contentTop.png") #if display private #end class __ASSET__img_ui_popup_contenttop_png extends lime.graphics.Image {}
@:image("assets/img/ui/popUp/title.png") #if display private #end class __ASSET__img_ui_popup_title_png extends lime.graphics.Image {}
@:image("assets/img/ui/topbarBg/topBarBg.png") #if display private #end class __ASSET__img_ui_topbarbg_topbarbg_png extends lime.graphics.Image {}
@:image("assets/img/ui/userIcon/UserIconBG.png") #if display private #end class __ASSET__img_ui_usericon_usericonbg_png extends lime.graphics.Image {}
@:image("assets/img/ui/userIcon/UserNoAvatarIcon.png") #if display private #end class __ASSET__img_ui_usericon_usernoavataricon_png extends lime.graphics.Image {}



#end
#end

#if (openfl && !flash)
@:keep #if display private #end class __ASSET__OPENFL__font_roboto_medium_ttf extends openfl.text.Font { public function new () { var font = new __ASSET__font_roboto_medium_ttf (); src = font.src; name = font.name; super (); }}
@:keep #if display private #end class __ASSET__OPENFL__font_roboto_regular_ttf extends openfl.text.Font { public function new () { var font = new __ASSET__font_roboto_regular_ttf (); src = font.src; name = font.name; super (); }}
@:keep #if display private #end class __ASSET__OPENFL__font_robotoslab_bold_ttf extends openfl.text.Font { public function new () { var font = new __ASSET__font_robotoslab_bold_ttf (); src = font.src; name = font.name; super (); }}
@:keep #if display private #end class __ASSET__OPENFL__font_robotoslab_regular_ttf extends openfl.text.Font { public function new () { var font = new __ASSET__font_robotoslab_regular_ttf (); src = font.src; name = font.name; super (); }}

#end

#end
